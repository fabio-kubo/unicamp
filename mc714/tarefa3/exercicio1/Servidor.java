import java.net.*;

public class Servidor {
     
   public static void main(String[] args) throws Exception{
      
      DatagramSocket serverSocket = new DatagramSocket(9876);
      byte[] mensagemRecebidaBytes;
      byte[] mensagemInvertidaBytes;
      System.out.println("...Inicializando server");

      //executa forever
      for(;;){
         mensagemRecebidaBytes = new byte[1024];
         mensagemInvertidaBytes = new byte[1024];

         //recebe mensagem do cliente
         DatagramPacket pacoteRecebido = new DatagramPacket(mensagemRecebidaBytes, mensagemRecebidaBytes.length);
         System.out.println("...Esperando mensagem do cliente");
         serverSocket.receive(pacoteRecebido);
         String mensagemCliente = new String( pacoteRecebido.getData());
         System.out.println("Recebeu do cliente: " + mensagemCliente);
         InetAddress IPAddress = pacoteRecebido.getAddress();
         int porta = pacoteRecebido.getPort();
           
         //Inverte a mensagem
         StringBuilder stringBuilder = new StringBuilder(mensagemCliente);
         String mensagemInvertida = stringBuilder.reverse().toString();
         mensagemInvertidaBytes = mensagemInvertida.getBytes();

         //Envia a mensagem invertida para o cliente
         DatagramPacket pacoteEnvio = new DatagramPacket(mensagemInvertidaBytes, mensagemInvertidaBytes.length, IPAddress, porta);
         serverSocket.send(pacoteEnvio);
      }
  }
}