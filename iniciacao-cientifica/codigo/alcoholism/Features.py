import numpy as np
import math as math
from scipy.signal import argrelmin
from scipy.signal import argrelmax

def get_area(data_wavelet):

	area = []
	for item in data_wavelet:
		area.append(np.mean(np.abs(item)))

	return area

def get_normalizedDecay(data_wavelet):

	normalizedDecay = []

	for item in data_wavelet:
		sumAux = 0
		for i in range(0, len(item)-1):
			sumAux+= 0.5 if (item[i+1]-item[i]) < 0 else -0.5
		normalizedDecay.append(np.abs(sumAux/(len(item)-1)))

	return normalizedDecay

def get_lineLength(data_wavelet):
	lineLength = []

	for item in data_wavelet:
		sumAux = 0
		for i in range(1, len(item)):
			sumAux+= abs(item[i] - item[i-1])
		lineLength.append(sumAux)

	return lineLength

def get_meanEnergy(data_wavelet):
	meanEnergy = []

	for item in data_wavelet:
		meanEnergy.append(np.mean(np.power(item, 2)))

	return meanEnergy;

def get_averagePeakAmplitude(data_wavelet):
	peakAmplitude = []

	for item in data_wavelet:
		#if doesn't exist peak
		if len(argrelmax(item)[0]) == 0:
			peakAmplitude.append(0)
		else:
			peaks = item[argrelmax(item)[0]]
			peakAmplitude.append(math.log10(np.mean(np.power(peaks, 2))))

	return peakAmplitude

def get_averageValleyAmplitude(data_wavelet):
	averageValleyAmplitude = []

	for item in data_wavelet:
		#if doesn't exist valley
		if len(argrelmin(item)[0]) == 0:
			averageValleyAmplitude.append(0)
		else:
			valleys = item[argrelmin(item)[0]]
			averageValleyAmplitude.append(math.log10(np.mean(np.power(valleys, 2))))

	return averageValleyAmplitude

def get_normalizedPeakNumber(data_wavelet):
	normalizedPeakNumber = []

	for item in data_wavelet:
		differences =[]
		nr_peaks = len(argrelmax(item)[0])
		for i in range(0, len(item)-1):
			differences.append(abs(item[i+1]-item[i]))

		if np.mean(differences) == 0:
			normalizedPeakNumber.append(0)
		else:
			normalizedPeakNumber.append(math.pow(np.mean(differences),-1)*nr_peaks)

	return normalizedPeakNumber

def get_peakVariation(data_wavelet):
	peakVariation = []
	variationIndice = []
	variationValue = []

	for item in data_wavelet:
		peaksIndices = argrelmax(item)[0]
		valleysIndices = argrelmin(item)[0]

		if len(peaksIndices) == 0 or len(valleysIndices) == 0:
			peakVariation.append(0)
		else:
			variationIndice = [abs(p-v) for p, v in zip(peaksIndices, valleysIndices)]
			peakVariation.append(np.std(variationIndice))

	return peakVariation

def get_rootMeanSquare(data_wavelet):
	rootMeanSquare = []

	for item in data_wavelet:
		rootMeanSquare.append(math.sqrt(np.mean(np.power(item, 2))))

	return rootMeanSquare

def get_waveletEnergy(data_wavelet):
	waveletEnergy = []

	#getting only details values
	for item in data_wavelet[1:]:
		waveletEnergy.append(np.mean(np.power(item, 2)))

	return waveletEnergy


def get_standartVariation(data_wavelet):
	standartVariation = []

	for item in data_wavelet:
		standartVariation.append(np.std(item))

	return standartVariation

def get_features_matrix(subjects):

	features_matrix = []
	labels = [] #1-alcoholic 0-control

	for subject in subjects:
		for subject_trials in zip(subject.trials_S1Obj, subject.trials_S2Match, subject.trials_S2NoMatch):
			aux_features_line = []
			for trial_matching_condition in subject_trials:
				for sensor in trial_matching_condition.eeg_signals:
					area = get_area(trial_matching_condition.eeg_signals[sensor])
					normalizedDecay = get_normalizedDecay(trial_matching_condition.eeg_signals[sensor])
					lineLength = get_lineLength(trial_matching_condition.eeg_signals[sensor])
					meanEnergy = get_meanEnergy(trial_matching_condition.eeg_signals[sensor])
					peakAmplitude = get_averagePeakAmplitude(trial_matching_condition.eeg_signals[sensor])
					valleyAmplitude = get_averageValleyAmplitude(trial_matching_condition.eeg_signals[sensor])
					normalizedPeakNumber = get_normalizedPeakNumber(trial_matching_condition.eeg_signals[sensor])
					peakVariation = get_peakVariation(trial_matching_condition.eeg_signals[sensor])
					rootMeanSquare = get_rootMeanSquare(trial_matching_condition.eeg_signals[sensor])
					waveletEnergy = get_waveletEnergy(trial_matching_condition.eeg_signals[sensor])
					standartVariation = get_standartVariation(trial_matching_condition.eeg_signals[sensor])
	
					aux_features_line += area + normalizedDecay + lineLength + meanEnergy + peakAmplitude+valleyAmplitude + peakVariation + rootMeanSquare + waveletEnergy + standartVariation
		
			labels.append(subject.isAlcoholic)
			features_matrix.append(aux_features_line);

	return features_matrix, labels
