import java.net.*;

public class Servidor{

   private static class ProcessaMensagem implements Runnable{
      
      private DatagramPacket pacoteRecebido;
      private DatagramSocket serverSocket;

      public ProcessaMensagem(DatagramPacket pct, DatagramSocket socket){
         this.pacoteRecebido = pct;
         this.serverSocket = socket;
      }


      public void run(){

         try{
            byte[] mensagemInvertidaBytes;
            String mensagemCliente = new String( this.pacoteRecebido.getData());
            System.out.println("Recebeu do cliente: " + mensagemCliente);
            InetAddress IPAddress = this.pacoteRecebido.getAddress();
            int porta = this.pacoteRecebido.getPort();
              
            //Inverte a mensagem
            StringBuilder stringBuilder = new StringBuilder(mensagemCliente);
            String mensagemInvertida = stringBuilder.reverse().toString();
            mensagemInvertidaBytes = mensagemInvertida.getBytes();

            //Envia a mensagem invertida para o cliente
            DatagramPacket pacoteEnvio = new DatagramPacket(mensagemInvertidaBytes, mensagemInvertidaBytes.length, IPAddress, porta);
            serverSocket.send(pacoteEnvio);
         }
         catch(Exception e){
            System.out.println(e.getMessage());
         }
      }
   }
     
   public static void main(String[] args) throws Exception{
      DatagramSocket serverSocket = new DatagramSocket(9876);
      byte[] mensagemRecebidaBytes;
      
      System.out.println("...Server funcionando");

      //executa forever
      for(;;){
         mensagemRecebidaBytes = new byte[1024];
         
         //recebe mensagem do cliente
         DatagramPacket pacoteRecebido = new DatagramPacket(mensagemRecebidaBytes, mensagemRecebidaBytes.length);
         serverSocket.receive(pacoteRecebido);

         Thread thread = new Thread(new ProcessaMensagem(pacoteRecebido, serverSocket));
         thread.start();
      }
  }
}