%Fabio Tadashi Kaneiwa Kubo RA: 145979


%Carrega os frames e converte para escala de cinza
function frames = load_frames_gray()
	srcFiles = dir('/home/cc2013/ra145979/unicamp/mc940/laboratorio2/images/xylophone/*.png');  % the folder in which ur images exists
	
	%pega a primeira imagem para saber a dimensao da matriz 3d
	filename = strcat('/home/cc2013/ra145979/unicamp/mc940/laboratorio2/images/xylophone/',srcFiles(1).name);
	imagem = rgb2gray(imread(filename));
	frames = zeros(size(imagem, 1), size(imagem, 2), length(srcFiles));
	frames(:,:,1) = imagem;

	for i = 2 : length(srcFiles)
	    filename = strcat('/home/cc2013/ra145979/unicamp/mc940/laboratorio2/images/xylophone/',srcFiles(i).name);
	    frames(:,:,i) = rgb2gray(imread(filename));
	end

end

%1.1 Diferencas entre Pixels
function contador = get_diferenca_contagem(video)

	limiar_t1 = 15 % define a variacao limite para ser um pixel ser considerado igual ao outro
	limiar_t2 = 4700 % define a quantidade maxima de pixels diferentes para ser considerado nao-significativamente diferente

	for i = 1 : size(video, 3) -1
		diferenca = abs(video(:,:,i) - video(:,:,i+1));
		maiores_que_limiar = diferenca > limiar_t1;
		contador(i) = sum(sum(maiores_que_limiar));
	end

	figure, plot(contador, 'b', ones(size(contador)) * limiar_t2, 'r'), title('Exercicio 1.1'), xlabel('Transicao'), ylabel('Medida'), legend('Medida', 'Limiar T2'); print -djpg grafico1_1.jpg
end

%1.2 Diferencas Estatısticas
function qtd_quadros_diferentes = get_diferenca_estatistica(video)

	dimensao_x = size(video,1) / 16;
	dimensao_y = size(video,2) / 16;
	
	limiar_t1 = (dimensao_x * dimensao_y)* 36 % define o erro quadratico acumulado maximo para ser considerado nao-significativamente diferente
	limiar_t2 = 80 % define a quantidade maximas de blocos significativamente diferentes para não ser considerado transição abrupta


	for i = 1 : size(video, 3) -1

		erro_quadrado = power(video(:,:,i) - video(:,:,i+1), 2);
		
		j=1;
		for l=1:16
			for c=1:16
				erro(j) = sum(sum(erro_quadrado( (l-1)*dimensao_x+1 : l*dimensao_x, 
											 (c-1)*dimensao_y+1 : c*dimensao_y)));
				j=j+1;
			end
		end

		qtd_quadros_diferentes(i) = sum(erro > limiar_t1);
	end

	figure, plot(qtd_quadros_diferentes, 'b', ones(size(qtd_quadros_diferentes)) * limiar_t2, 'r'), title('Exercício 1.2'), xlabel('Transições'), ylabel('Medida'), legend('Medida', 'Limiar T2'); print -djpg grafico1_2.jpg

end


%1.3 Histogramas
function diferenca_acumulada = get_diferenca_histograma(video)

	for i = 1 : size(video, 3)-1
		histogramaA = imhist(uint8(video(:,:,i)), 32);
		histogramaB = imhist(uint8(video(:,:,i+1)), 32);
		diferenca_acumulada(i) = sum(abs(histogramaA - histogramaB));
	end

	valor_medio = mean(diferenca_acumulada)
	desvio_padrao = std(diferenca_acumulada)

	limiar_t = valor_medio + desvio_padrao*2;

	figure, plot(diferenca_acumulada, 'b', ones(size(diferenca_acumulada)) * limiar_t, 'r'), title('Exercício 1.3'), xlabel('Transições'), ylabel('Medida'), legend('Medida', 'Limiar T'); print -djpg grafico1_3.jpg
end