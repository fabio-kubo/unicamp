Scenario: Looking up the definition of a word
Given the user is on the Wikionary home page
When the user looks up the definition of the word <word>
Then they should see the definition <definition>

Examples:    
|word|definition|
|apple|A common, round fruit|
|pear|An edible fruit produced by the pear tree|
|tomato|A widely cultivated plant, Solanum lycopersicum|
|cucumber|A vine in the gourd family, Cucumis sativus|


Scenario: Looking up the definition of 'suny'
Given the user is on the Wikionary home page
When the user looks up the definition of the word 'suny'
Then they should see an error 'Did you mean: sunny'
