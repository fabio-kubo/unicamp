%Fabio Tadashi Kaneiwa Kubo 145979

%---------------------------------VIZINHO MAIS PROXIMO -----------------------------------------------------------------------
function imagem_processada = transformacao_escala_VizinhoMaisProximo(fator_escala, dimensao, imagem_entrada, nome_saida)
    
    imagem_processada = zeros(dimensao(1), dimensao(2), 3);

    for i = 1 : size(imagem_processada, 1)
        for j = 1 : size(imagem_processada, 2)
            x_linha = round(i/fator_escala);
            y_linha = round(j/fator_escala);
            if (x_linha <= size(imagem_entrada, 1) && y_linha <= size(imagem_entrada, 2))
                imagem_processada(i, j, :) = imagem_entrada(x_linha, y_linha, :);
            endif
        end
    end

    %salva imagem
    imwrite(uint8(imagem_processada), strcat(nome_saida, ".ppm"));
    imwrite(uint8(imagem_processada), strcat(nome_saida, ".png"));
end

function imagem_processada = transformacao_rotacao_VizinhoMaisProximo(angulo, dimensao, imagem_entrada, nome_saida)

    imagem_processada = zeros(dimensao(1), dimensao(2), 3);

    angulo_radianos = angulo*pi/180;
    cosseno = cos(angulo_radianos);
    seno = sin(angulo_radianos);
    origem_i = size(imagem_entrada, 1)/2;
    origem_j = size(imagem_entrada, 2)/2;

    for i = 1 : size(imagem_processada, 1)
        for j = 1 : size(imagem_processada, 2)
                x_linha = round((i-origem_i)*cosseno + (j-origem_j)*seno + origem_i);
            y_linha = round((j-origem_j)*cosseno - (i-origem_i)*seno + origem_j);
            if (x_linha <= size(imagem_entrada, 1) && x_linha > 0 
                && y_linha <= size(imagem_entrada, 2) && y_linha > 0)
                imagem_processada(i, j, :) = imagem_entrada(x_linha, y_linha, :);
            endif
        end
    end

    %salva imagem
        imwrite(uint8(imagem_processada), strcat(nome_saida, ".ppm"));
        imwrite(uint8(imagem_processada), strcat(nome_saida, ".png"));
end

%---------------------------------INTERPOLACAO BILINEAR-----------------------------------------------------------------------
function imagem_processada = transformacao_escala_Bilinear(fator_escala, dimensao, imagem_entrada, nome_saida)
    
    imagem_processada = zeros(dimensao(1), dimensao(2), 3);

    for i = 1 : size(imagem_processada, 1)
        for j = 1 : size(imagem_processada, 2)
            x_linha = i/fator_escala + 1;
            x = floor(x_linha);
            dx = x_linha - x;

            y_linha = j/fator_escala + 1;
            y = floor(y_linha);
            dy = y_linha - y;

            if (x < size(imagem_entrada, 1) && y < size(imagem_entrada, 2))
                imagem_processada(i, j, :) = (1-dx)*(1-dy)*imagem_entrada(x, y,:) + dx*(1-dy)*imagem_entrada(x+1,y,:) + (1-dx)*dy*imagem_entrada(x,y+1,:) + dx*dy*imagem_entrada(x+1, y+1);
            end
        end
    end

    %salva imagem
    imwrite(uint8(imagem_processada), strcat(nome_saida, ".ppm"));
    imwrite(uint8(imagem_processada), strcat(nome_saida, ".png"));
end

function imagem_processada = transformacao_rotacao_Bilinear(angulo, dimensao, imagem_entrada, nome_saida)

    imagem_processada = zeros(dimensao(1), dimensao(2), 3);

    angulo_radianos = angulo*pi/180;
    cosseno = cos(angulo_radianos);
    seno = sin(angulo_radianos);
    origem_i = size(imagem_entrada, 1)/2;
    origem_j = size(imagem_entrada, 2)/2;

    for i = 1 : size(imagem_processada, 1)
        for j = 1 : size(imagem_processada, 2)

            x_linha = (i-origem_i)*cosseno + (j-origem_j)*seno + origem_i;
            x = floor(x_linha);
            dx = x_linha - x;

            y_linha = (j-origem_j)*cosseno - (i-origem_i)*seno + origem_j + 1;
            y = floor(y_linha);
            dy = y_linha - y;

        if (x < size(imagem_entrada, 1) && x > 0 
                && y < size(imagem_entrada, 2) && y > 0)
                imagem_processada(i, j, :) = (1-dx)*(1-dy)*imagem_entrada(x, y,:) + dx*(1-dy)*imagem_entrada(x+1,y,:) + (1-dx)*dy*imagem_entrada(x,y+1,:) + dx*dy*imagem_entrada(x+1, y+1);
            end
        end
    end

    %salva imagem
    imwrite(uint8(imagem_processada), strcat(nome_saida, ".ppm"));
    imwrite(uint8(imagem_processada), strcat(nome_saida, ".png"));
end

%---------------------------------INTERPOLACAO BICUBICA-----------------------------------------------------------------------

function r = R(s)

    r = (power(P(s+2), 3) - 4*power(P(s+1), 3) + 6*power(P(s), 3) -4*power(P(s-1),3))/6;
end

function p = P(t)
    if t > 0
        p = t;
    else
        p = 0;
    endif
end

function imagem_processada = transformacao_escala_Bicubica(fator_escala, dimensao, imagem_entrada, nome_saida)
    
    imagem_processada = zeros(dimensao(1), dimensao(2), 3);

    for i = 1 : size(imagem_processada, 1)
        for j = 1 : size(imagem_processada, 2) 
            x_linha = i/fator_escala + 1;
            x = floor(x_linha);
            dx = x_linha - x;

            y_linha = j/fator_escala + 1;
            y = floor(y_linha);
            dy = y_linha - y;

            if((x-1) >=1 && (y-1) >=1 && (x+2) <= size(imagem_entrada, 1) && (y+2) <= size(imagem_entrada, 2))
                soma = 0;
                for m = -1 : 2
                    for n = -1 : 2
                        soma += imagem_entrada(x+m, y+n,:)*R(m-dx)*R(dy-n);
                    end
                end
                
                imagem_processada(i, j, :) = soma;
            endif

        end
    end

    %salva imagem
    imwrite(uint8(imagem_processada), strcat(nome_saida, ".ppm"));
    imwrite(uint8(imagem_processada), strcat(nome_saida, ".png"));
end

function imagem_processada = transformacao_rotacao_Bicubica(angulo, dimensao, imagem_entrada, nome_saida)
    
    imagem_processada = zeros(dimensao(1), dimensao(2), 3);

    angulo_radianos = angulo*pi/180;
    cosseno = cos(angulo_radianos);
    seno = sin(angulo_radianos);
    origem_i = size(imagem_entrada, 1)/2;
    origem_j = size(imagem_entrada, 2)/2;

    for i = 1 : size(imagem_processada, 1)
        for j = 1 : size(imagem_processada, 2) 
            x_linha = (i-origem_i)*cosseno + (j-origem_j)*seno + origem_i;
            x = floor(x_linha);
            dx = x_linha - x;

            y_linha = (j-origem_j)*cosseno - (i-origem_i)*seno + origem_j + 1;
            y = floor(y_linha);
            dy = y_linha - y;

            if((x-1) >=1 && (y-1) >=1 && (x+2) <= size(imagem_entrada, 1) && (y+2) <= size(imagem_entrada, 2))
                soma = 0;
                for m = -1 : 2
                    for n = -1 : 2
                        soma += imagem_entrada(x+m, y+n,:)*R(m-dx)*R(dy-n);
                    end
                end
                
                imagem_processada(i, j, :) = soma;
            endif

        end
    end

    %salva imagem
    imwrite(uint8(imagem_processada), strcat(nome_saida, ".ppm"));
    imwrite(uint8(imagem_processada), strcat(nome_saida, ".png"));
end

%---------------------------------INTERPOLACAO POLINOMIOS DE LAGRANGE----------------------------------------------------------

function l = L(n, imagem, x, y, dx, dy)

    l = (-dx*(dx-1)*(dx-2)*double(imagem(x-1, y+n-2,:)))/6 + ((dx+1)*(dx-1)*(dx-2)*double(imagem(x, y+n-2,:)))/2 + (-dx*(dx+1)*(dx-2)*double(imagem(x+1, y+n-2, :)))/2 + (dx*(dx+1)*(dx-1)*double(imagem(x+2, y+n-2, :)))/6;
end

function imagem_processada = transformacao_escala_PolinomiosLagrange(fator_escala, dimensao, imagem_entrada, nome_saida)
    
    imagem_processada = zeros(dimensao(1), dimensao(2), 3);

    for i = 1 : size(imagem_processada, 1)
        for j = 1 : size(imagem_processada, 2)
            
            x_linha = i/fator_escala + 1;
            x = floor(x_linha);
            dx = x_linha - x;

            y_linha = j/fator_escala + 1;
            y = floor(y_linha);
            dy = y_linha - y;

            if ((x+2) <= size(imagem_entrada, 1) && (x-1) > 0 
                && (y+2) < size(imagem_entrada, 2) && (y-1) > 0)
                imagem_processada(i, j, :) = (-dy*(dy-1)*(dy-2)*L(1, imagem_entrada, x, y, dx, dy))/6 + ((dy+1)*(dy-1)*(dy-2)*L(2, imagem_entrada, x, y, dx, dy))/2 + (-dy*(dy+1)*(dy-2)*L(3, imagem_entrada, x, y, dx, dy))/2 + (dy*(dy +1)*(dy-1)*L(4, imagem_entrada, x, y, dx, dy))/6;
            endif
        end
    end

    %salva imagem
    imwrite(uint8(imagem_processada), strcat(nome_saida, ".ppm"));
    imwrite(uint8(imagem_processada), strcat(nome_saida, ".png"));
end

function imagem_processada = transformacao_rotacao_PolinomiosLagrange(angulo, dimensao, imagem_entrada, nome_saida)
    
    imagem_processada = zeros(dimensao(1), dimensao(2), 3);

    angulo_radianos = angulo*pi/180;
    cosseno = cos(angulo_radianos);
    seno = sin(angulo_radianos);
    origem_i = size(imagem_entrada, 1)/2;
    origem_j = size(imagem_entrada, 2)/2;

    for i = 1 : size(imagem_processada, 1)
        for j = 1 : size(imagem_processada, 2)
            
            x_linha = (i-origem_i)*cosseno + (j-origem_j)*seno + origem_i;
            x = floor(x_linha);
            dx = x_linha - x;

            y_linha = (j-origem_j)*cosseno - (i-origem_i)*seno + origem_j + 1;
            y = floor(y_linha);
            dy = y_linha - y;

            if ((x+2) <= size(imagem_entrada, 1) && (x-1) > 0 
                && (y+2) < size(imagem_entrada, 2) && (y-1) > 0)
                imagem_processada(i, j, :) = (-dy*(dy-1)*(dy-2)*L(1, imagem_entrada, x, y, dx, dy))/6 + ((dy+1)*(dy-1)*(dy-2)*L(2, imagem_entrada, x, y, dx, dy))/2 + (-dy*(dy+1)*(dy-2)*L(3, imagem_entrada, x, y, dx, dy))/2 + (dy*(dy +1)*(dy-1)*L(4, imagem_entrada, x, y, dx, dy))/6;
            endif
        end
    end

    %salva imagem
    imwrite(uint8(imagem_processada), strcat(nome_saida, ".ppm"));
    imwrite(uint8(imagem_processada), strcat(nome_saida, ".png"));
end

%---------------------------------MAIN-----------------------------------------------------------------------------------------

function imagem_transformada = main(angulo, fator_escala, dimensao, tipo_interpolacao, imagem_entrada, nome_saida)
    if (fator_escala != 1 && angulo !=0)
        disp('Erro: o metodo efetua uma transformacao geometrica por vez.')
    elseif (fator_escala != 1)
        if(tipo_interpolacao == 1)
            imagem_transformada = transformacao_escala_VizinhoMaisProximo(fator_escala, dimensao, imagem_entrada, nome_saida);
        elseif(tipo_interpolacao == 2)
            imagem_transformada = transformacao_escala_Bilinear(fator_escala, dimensao, imagem_entrada, nome_saida);
        elseif(tipo_interpolacao == 3)
            imagem_transformada = transformacao_escala_Bicubica(fator_escala, dimensao, imagem_entrada, nome_saida);
        elseif(tipo_interpolacao == 4)
            imagem_transformada = transformacao_escala_PolinomiosLagrange(fator_escala, dimensao, imagem_entrada, nome_saida);
        endif
    else
        if(tipo_interpolacao == 1)
            imagem_transformada = transformacao_rotacao_VizinhoMaisProximo(angulo, dimensao, imagem_entrada, nome_saida);
        elseif(tipo_interpolacao == 2)
            imagem_transformada = transformacao_rotacao_Bilinear(angulo, dimensao, imagem_entrada, nome_saida);
        elseif(tipo_interpolacao == 3)
            imagem_transformada = transformacao_rotacao_Bicubica(angulo, dimensao, imagem_entrada, nome_saida);
        elseif(tipo_interpolacao == 4)
            imagem_transformada = transformacao_rotacao_PolinomiosLagrange(angulo, dimensao, imagem_entrada, nome_saida);
        endif
    endif

end