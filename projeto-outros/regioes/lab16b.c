//	ALUNO: Carolina Clepf Pagotto- RA: 157770
//	Lab 16
//	
//	O programa altera a cor de uma regiao da imagem a partir de um metodo recursivo,
// 	em que primeiro altera o pixel local e depois faz o mesmo para seus oito vizinhos.
//-----------------------------------------------------------------------------------------------------------
#include <stdlib.h>
#include <stdio.h>

//Metodo que imprime a imagem
void imprimir_imagem(int imagem[200][200], int largura, int altura);

//metodo que altera a cor de uma região
void alterar_cor_regiao(int imagem[200][200], int larguraIm, int alturaIm, int y, int x, int cor_alteracao, int cor_anterior);

int main(int argc, char *argv[]){

	FILE * arqImagem, * arqParam;
	int imagem[200][200];
	int cor_alteracao, cor_anterior, larguraIm, alturaIm, x, y, i, j;
	char descarte[5];

	//abre arquivo da imagem
	arqImagem = fopen(argv[1],"r");

	if(!arqImagem){
		printf("Erro ao abrir o arquivo de imagem.\n");
		exit(EXIT_FAILURE);
	}

	//le a primeira linha - descarta
	fscanf(arqImagem, "%s\n", descarte);

	//le as dimensoes
	fscanf(arqImagem, "%d %d\n", &larguraIm, &alturaIm);

	//le a terceira linha - descarta
	fscanf(arqImagem, "%s\n", descarte);

	//preenche a matriz
	for (i = 0; i < alturaIm; i++){
		for (j = 0; j < larguraIm; j++){
			fscanf(arqImagem, "%d ", &imagem[i][j]);
		}
	}

	//abre arquivo do parametros
	arqParam = fopen(argv[2],"r");
	if(!arqImagem){
		printf("Erro ao abrir o arquivo de parametros.\n");
		exit(EXIT_FAILURE);
	}

	//carrega os parametros
	fscanf(arqParam, "%d\n", &y);
	fscanf(arqParam, "%d\n", &x);
	fscanf(arqParam, "%d\n", &cor_alteracao);

	cor_anterior = imagem[y][x];
	//altera a cor de uma regiao
	alterar_cor_regiao(imagem, larguraIm, alturaIm, y, x, cor_alteracao, cor_anterior);

	//imprime a imagem
	imprimir_imagem(imagem, larguraIm, alturaIm);
	
	return 0;
}

void imprimir_imagem(int imagem[200][200], int largura, int altura){

	int i, j;

	//imprime o cabecalho
	printf("P2\n%d %d\n255\n", largura, altura);

	//imprime a matriz da imagem
	for (i = 0; i < altura; i++){
		for (j = 0; j < largura; j++){
			printf("%d ", imagem[i][j]);
		}
		printf("\n");
	}
}

void alterar_cor_regiao(int imagem[200][200], int larguraIm, int alturaIm, int y, int x, int cor_alteracao, int cor_anterior){

	int existe_coluna_anterior, existe_coluna_posterior, existe_linha_anterior, existe_linha_posterior;

	existe_coluna_anterior = (x-1) >=0;
	existe_coluna_posterior = (x+1) < larguraIm;
	existe_linha_anterior = (y-1) >= 0;
	existe_linha_posterior = (y+1) < alturaIm;

	//se o pixel atual nao pertence a regiao, da return
	if(cor_anterior != imagem[y][x]){
		return;
	}

	/*altera a cor do pixel atual*/
	imagem[y][x] = cor_alteracao;

	/*aplica recursao para os vizinhos
		Esquema dos vizinhos (X eh o pixel atual):
		0 1 2
		3 X 4
		5 6 7
	*/

	//primeira linha
	if(existe_linha_anterior){
		// vizinho 0
		if(existe_coluna_anterior){
			alterar_cor_regiao(imagem, larguraIm, alturaIm, y-1, x-1, cor_alteracao, cor_anterior);
		}

		// vizinho 1
		alterar_cor_regiao(imagem, larguraIm, alturaIm, y-1, x, cor_alteracao, cor_anterior);

		// vizinho 2
		if(existe_coluna_posterior){
			alterar_cor_regiao(imagem, larguraIm, alturaIm, y-1, x+1, cor_alteracao, cor_anterior);
		}
	}

	// vizinho 3
	if(existe_coluna_anterior){
		alterar_cor_regiao(imagem, larguraIm, alturaIm, y, x-1, cor_alteracao, cor_anterior);
	}

	// vizinho 4
	if((x+1) < larguraIm){
		alterar_cor_regiao(imagem, larguraIm, alturaIm, y, x+1, cor_alteracao, cor_anterior);
	}

	//terceira linha
	if(existe_linha_posterior){
		// vizinho 5
		if(existe_coluna_anterior){
			alterar_cor_regiao(imagem, larguraIm, alturaIm, y+1, x-1, cor_alteracao, cor_anterior);
		}

		// vizinho 6
		alterar_cor_regiao(imagem, larguraIm, alturaIm, y+1, x, cor_alteracao, cor_anterior);

		// vizinho 7
		if(existe_coluna_posterior){
			alterar_cor_regiao(imagem, larguraIm, alturaIm, y+1, x+1, cor_alteracao, cor_anterior);
		}
	}
}