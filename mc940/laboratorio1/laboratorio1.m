%1.1 - Combinacao de Imagens
function f = combinacao_imagens(imA, imB)
    im_preto_brancoA = rgb2gray(imA);
    im_preto_brancoB = rgb2gray(imB);
    f = 0.5*im_preto_brancoA + 0.5*im_preto_brancoB;
    imwrite(f, "combinacao_imagem.pgm");
end


%1.2 Planos de Bits
function p = get_planos_bits(im)
    im_preto_branco =  rgb2gray(im);

    p = zeros(size(im_preto_branco, 1), size(im_preto_branco, 2), 8);
    
    p(:,:,1) = bitget(im_preto_branco, 1);
    p(:,:,2) = bitget(im_preto_branco, 2);
    p(:,:,3) = bitget(im_preto_branco, 3);
    p(:,:,4) = bitget(im_preto_branco, 4);
    p(:,:,5) = bitget(im_preto_branco, 5);
    p(:,:,6) = bitget(im_preto_branco, 6);
    p(:,:,7) = bitget(im_preto_branco, 7);
    p(:,:,8) = bitget(im_preto_branco, 8);

    imwrite(p(:,:,1), "plano_1.pgm");
    imwrite(p(:,:,2), "plano_2.pgm");
    imwrite(p(:,:,3), "plano_3.pgm");
    imwrite(p(:,:,4), "plano_4.pgm");
    imwrite(p(:,:,5), "plano_5.pgm");
    imwrite(p(:,:,6), "plano_6.pgm");
    imwrite(p(:,:,7), "plano_7.pgm");
    imwrite(p(:,:,8), "plano_8.pgm");

end


%1.3 Comparacao entre imagens
function comparar_imagens(imA, imB)

    %Com 4 bins
    histogramaA_1 = imhist(imA(:,:,1), 4);
    histogramaA_2 = imhist(imA(:,:,2), 4);
    histogramaA_3 = imhist(imA(:,:,3), 4);

    histogramaB_1 = imhist(imB(:,:,1), 4);
    histogramaB_2 = imhist(imB(:,:,2), 4);
    histogramaB_3 = imhist(imB(:,:,3), 4);

    %normalizacao
    histogramaA_1 = histogramaA_1/sum(histogramaA_1);
    histogramaA_2 = histogramaA_2/sum(histogramaA_2);
    histogramaA_3 = histogramaA_3/sum(histogramaA_3);
    histogramaB_1 = histogramaB_1/sum(histogramaB_1);
    histogramaB_2 = histogramaB_2/sum(histogramaB_2);
    histogramaB_3 = histogramaB_3/sum(histogramaB_3);

    %Calculando a distancia euclidiana
    distancia_1 = norm(histogramaA_1 - histogramaB_1);
    distancia_2 = norm(histogramaA_2 - histogramaB_2);
    distancia_3 = norm(histogramaA_3 - histogramaB_3);

    disp('Distancia euclidiana 4 bins: ');
    disp((distancia_1+distancia_2+distancia_3)/3);

    %Com 32 bins

    histogramaC_1 = imhist(imA(:,:,1), 32);
    histogramaC_2 = imhist(imA(:,:,2), 32);
    histogramaC_3 = imhist(imA(:,:,3), 32);

    histogramaD_1 = imhist(imB(:,:,1), 32);
    histogramaD_2 = imhist(imB(:,:,2), 32);
    histogramaD_3 = imhist(imB(:,:,3), 32);

    %normalizacao
    histogramaC_1 = histogramaC_1/sum(histogramaC_1);
    histogramaC_2 = histogramaC_2/sum(histogramaC_2);
    histogramaC_3 = histogramaC_3/sum(histogramaC_3);
    histogramaD_1 = histogramaD_1/sum(histogramaD_1);
    histogramaD_2 = histogramaD_2/sum(histogramaD_2);
    histogramaD_3 = histogramaD_3/sum(histogramaD_3);

    %Calculando a distancia euclidiana
    distancia_1 = norm(histogramaC_1 - histogramaD_1);
    distancia_2 = norm(histogramaC_2 - histogramaD_2);
    distancia_3 = norm(histogramaC_3 - histogramaD_3);
    
    disp('Distancia euclidiana 32 bins: ');
    disp((distancia_1+distancia_2+distancia_3)/3);

    %Com 128 bins

    histogramaE_1 = imhist(imA(:,:,1), 128);
    histogramaE_2 = imhist(imA(:,:,2), 128);
    histogramaE_3 = imhist(imA(:,:,3), 128);

    histogramaF_1 = imhist(imB(:,:,1), 128);
    histogramaF_2 = imhist(imB(:,:,2), 128);
    histogramaF_3 = imhist(imB(:,:,3), 128);

    %normalizacao
    histogramaE_1 = histogramaE_1/sum(histogramaE_1);
    histogramaE_2 = histogramaE_2/sum(histogramaE_2);
    histogramaE_3 = histogramaE_3/sum(histogramaE_3);
    histogramaF_1 = histogramaF_1/sum(histogramaF_1);
    histogramaF_2 = histogramaF_2/sum(histogramaF_2);
    histogramaF_3 = histogramaF_3/sum(histogramaF_3);

    %Calculando a distancia euclidiana
    distancia_1 = norm(histogramaE_1 - histogramaF_1);
    distancia_2 = norm(histogramaE_2 - histogramaF_2);
    distancia_3 = norm(histogramaE_3 - histogramaF_3);
    disp('Distancia euclidiana 128 bins: ');
    disp((distancia_1+distancia_2+distancia_3)/3);

    %Com 256 bins

    histogramaG_1 = imhist(imA(:,:,1), 256);
    histogramaG_2 = imhist(imA(:,:,2), 256);
    histogramaG_3 = imhist(imA(:,:,3), 256);

    histogramaH_1 = imhist(imB(:,:,1), 256);
    histogramaH_2 = imhist(imB(:,:,2), 256);
    histogramaH_3 = imhist(imB(:,:,3), 256);

    %normalizacao
    histogramaG_1 = histogramaG_1/sum(histogramaG_1);
    histogramaG_2 = histogramaG_2/sum(histogramaG_2);
    histogramaG_3 = histogramaG_3/sum(histogramaG_3);
    histogramaH_1 = histogramaH_1/sum(histogramaH_1);
    histogramaH_2 = histogramaH_2/sum(histogramaH_2);
    histogramaH_3 = histogramaH_3/sum(histogramaH_3);

    %Calculando a distancia euclidiana
    distancia_1 = norm(histogramaG_1 - histogramaH_1);
    distancia_2 = norm(histogramaG_2 - histogramaH_2);
    distancia_3 = norm(histogramaG_3 - histogramaH_3);
    disp('Distancia euclidiana 256 bins: ');
    disp((distancia_1+distancia_2+distancia_3)/3);


    %exibe os histogramas
    subplot (2, 3, 1)
    imhist(imA(:,:,1), 32);

    subplot (2, 3, 2)
    imhist(imA(:,:,2), 32);

    subplot (2, 3, 3)
    imhist(imA(:,:,3), 32);
    
    subplot (2, 3, 4)
    imhist(imB(:,:,1), 32);

    subplot (2, 3, 5)
    imhist(imB(:,:,2), 32);

    subplot (2, 3, 6)
    imhist(imB(:,:,3), 32);

end

%1.4 Filtragem
function f = filtrar(im)
    im_preto_branco =  im2double(rgb2gray(im));
    filtro = 1/9 * ones(3);
    tamanho = size(im_preto_branco);
    
    %inicializa o retorno
    f = zeros(tamanho);

    for i = 2:tamanho(1)-1
        for j = 2:tamanho(2)-1
            produto = im_preto_branco(i-1:i+1,j-1:j+1) .* filtro;
            f(i,j) = sum(produto(:));
        end
    end

    imwrite(f, "filtrar.pgm");

end

%1.5 Mosaicos
function f = get_mosaico(im)
    im_preto_branco = rgb2gray(im);
    f = zeros(size(im_preto_branco));
    dimensao_x = size(im_preto_branco, 1) / 4;
    dimensao_y = size(im_preto_branco, 2) / 4;
    
    c = mat2cell(im_preto_branco, dimensao_x * ones(1, 4), dimensao_y * ones(1, 4));

    d = cell(size(c));

    d{1, 1} = c{1, 1};
    d{1, 2} = c{2, 2};
    d{1, 4} = c{3, 3};
    d{1, 3} = c{4, 4};

    d{2, 1} = c{1, 2};
    d{2, 2} = c{2, 3};
    d{2, 4} = c{3, 4};
    d{2, 3} = c{4, 1};

    d{3, 1} = c{1, 3};
    d{3, 2} = c{2, 4};
    d{3, 4} = c{3, 1};
    d{3, 3} = c{4, 2};
    
    d{4, 1} = c{1, 4};
    d{4, 2} = c{2, 1};
    d{4, 4} = c{3, 2};
    d{4, 3} = c{4, 3};

    f = cell2mat(d);

    imwrite(f, "mosaico.pgm");
end