import java.net.*;
import java.io.*;

public class Cliente {

	private Socket cliente;
	private DataOutputStream streamOut;
	static final int NR_PORTA = 12666;
	static final String IP = "localhost";

	public Cliente(){
	}
	
	public void estabelecerConexao() throws Exception{

		System.out.println("Estabelecendo conexao...");
		cliente = new Socket(IP, NR_PORTA);
		System.out.println("Conectado!");
	}

	public void iniciarTransmissao() throws Exception{

		BufferedReader buffer = new BufferedReader(new InputStreamReader(System.in));   
		
		streamOut = new DataOutputStream(cliente.getOutputStream());
		String textoCLiente = "";
		while (!textoCLiente.equals("exit")){ 
			textoCLiente = buffer.readLine();
			streamOut.writeBytes(textoCLiente+ '\n');
		}
	}

	public static void main(String args[]){
		
		Cliente cliente = new Cliente();

		try{
			cliente.estabelecerConexao();
			cliente.iniciarTransmissao();
		}
		catch(Exception e){
			System.out.println("Erro: " + e.getMessage());
		}
	}
}