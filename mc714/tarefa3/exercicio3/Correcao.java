import java.io.Serializable;

public class Correcao implements Serializable{
	
	private static final long serialVersionUID = 1L;
	private int idQuestao;
	private int qtdAcertos;
	private int qtdErros;

	public Correcao(int id, int qtdAcertos, int qtdErros){
		this.idQuestao = id;
		this.qtdAcertos = qtdAcertos;
		this.qtdErros = qtdErros;
	}

	public int getIdQuestao(){
		return this.idQuestao;
	}

	public int getQtdAcertos(){
		return this.qtdAcertos;
	}

	public int getQtdErros(){
		return this.qtdErros;
	}

	public void setIdQuestao(int novoId){
		this.idQuestao = novoId;
	}

	public void setQtdAcertos(int novaQtd){
		this.qtdAcertos = novaQtd;
	}

	public void setQtdErros(int novaQtd){
		this.qtdErros = novaQtd;
	}

	public String toStringFormatada(){
		return "Questão " + getIdQuestao() + ": acertos=" + getQtdAcertos() + " erros=" + getQtdErros();
	}

	public String toString(){
		return getIdQuestao() + ";" + getQtdAcertos() + ";" + getQtdErros();
	}
}