import matplotlib.pyplot as plt
import numpy as np

#Le os sinais
arqComSinais = open('../amostras/SetE/S001.txt')
sinais = map(int, arqComSinais.readlines()) #converte para inteiro

#seta os parametros
frequencia_amostragem = 173.61
quantidade_sinais = len(sinais)
periodo = 1/frequencia_amostragem

#lista de tempo - abscissa
eixo_tempo = np.linspace(0, quantidade_sinais-1, quantidade_sinais)*periodo   # time vector

plt.plot(eixo_tempo, sinais)
plt.ylabel('Voltage('+r'$\mu$'+'V)')
plt.xlabel('Time(s)')
plt.show()