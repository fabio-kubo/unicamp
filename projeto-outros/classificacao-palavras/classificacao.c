/*
	Laboratório Nº 14
	Classificação de Palavras

	Aluno: Gabriel Seixas Disselli
	Ra: 146165
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct Word {
	char palavra[80];
	int ocorrencias;
} Word; 

int contar_palavras(char * texto){
	int qtdPalavras, i, anteriorEraLetra;

	anteriorEraLetra = 0;
	qtdPalavras = 0;

	for(i = 0; texto[i] != '\0'; i++){
		if(texto[i] == ' '){

			if(anteriorEraLetra == 1){
				qtdPalavras++;
			}

			anteriorEraLetra = 0;
		}
		else{
			anteriorEraLetra = 1;
		}
	}

	return qtdPalavras;
}

void remover_pontuacao(char * texto){
	int i;

	for (i = 0; i < strlen(texto); i++){
		if(texto[i] == '.' || texto[i] == ',' || texto[i] == ':' || texto[i] == ';' || texto[i] == 34 || texto[i] == 39 || texto[i] == '!'
			|| texto[i] == '?'|| texto[i] == '(' || texto[i] == ')' || texto[i] == '\n'){
			texto[i] = ' ';
		}
	}
}

int buscar_palavra(char * palavraBuscada, Word * palavras, int qtdPalavras){
	int i;

	for (i = 0; i < qtdPalavras; i++){
		if(strcmp(palavras[i].palavra, palavraBuscada) == 0){
			return i;
		}
	}

	//caso não encontre
	return -1;
}

int contar_ocorrencias(Word * palavras, char texto[1001]){
	char * palavraAtual;
	int indice, qtdPalavras;

	qtdPalavras = 0;
	palavraAtual = strtok (texto," ");

	//percorre todas as palavras
	while (palavraAtual != NULL){

		indice = buscar_palavra(palavraAtual, palavras, qtdPalavras);

		//nova Palavra
		if(indice == -1){
			palavras[qtdPalavras].ocorrencias = 1;
			strcpy(palavras[qtdPalavras].palavra, palavraAtual);
			qtdPalavras++;
		}
		else{
			palavras[indice].ocorrencias++;
		}
    	
    	palavraAtual = strtok (NULL, " ");
	}

	return qtdPalavras;
}

void ordenar_palavras(Word * palavras, int qtdPalavras){
	int i, auxOcorrencia, trocou;
	char auxPalavra[80];

	do{

		trocou = 0;

		for (i = 1; i < qtdPalavras; i++){

			if(palavras[i-1].ocorrencias < palavras[i].ocorrencias){

				auxOcorrencia = palavras[i-1].ocorrencias;
				strcpy(auxPalavra, palavras[i-1].palavra);

				palavras[i-1].ocorrencias = palavras[i].ocorrencias;
				strcpy(palavras[i-1].palavra, palavras[i].palavra);

				palavras[i].ocorrencias = auxOcorrencia;
				strcpy(palavras[i].palavra, auxPalavra);

				trocou = 1;
			}
		}

	}
	while(trocou != 0);

}

void imprimir(Word * palavras, int qtdPalavras){
	int i;

	for (i = 0; i < qtdPalavras; i++){
		printf("\"%s\", %d\n", palavras[i].palavra, palavras[i].ocorrencias);
	}
}

int main(){

	Word * palavras;
	char texto[1001];
	int qtdTotal, qtdPalavras;

	fgets(texto, 1000, stdin);
	
	remover_pontuacao(texto);
	
	qtdTotal = contar_palavras(texto);

	//aloca dinamicamente o vetor de palavras
	palavras = (Word *) malloc(sizeof(Word)* qtdTotal);

	qtdPalavras = contar_ocorrencias(palavras, texto);

	//ordena decrescentemente
	ordenar_palavras(palavras, qtdPalavras);

	//imprime as palavras com suas respectivas ocorrencias
	imprimir(palavras, qtdPalavras);

	//desaloca
	free(palavras);
	
	return 0;
}

