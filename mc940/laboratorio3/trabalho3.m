%Fabio Tadashi Kaneiwa Kubo RA: 145979

%Função que converte a imagem na escala de cinza para um pontilhado de forma ordenada
function imagem_pontilhada = get_pontilhado_ordenado(imagem_entrada, nome)
	
	mascara = [6, 8, 4; 1, 0, 3; 5, 2, 7];

	%Normaliza a imagem com valores de 0 a 9
	imagem_normalizada = double(imagem_entrada) / 255 * 9;

	imagem_pontilhada = zeros(3*size(imagem_entrada));

	for i = 1 : size(imagem_entrada, 1)
		for j = 1 : size(imagem_entrada, 2)
			imagem_pontilhada(3*(i-1)+1:3*i, 3*(j-1)+1:3*j) = imagem_normalizada(i,j) > mascara;
		end
	end
	imwrite(logical(imagem_pontilhada), strcat(nome, "_get_pontilhado_ordenado.pbm"));
	imwrite(imagem_pontilhada, strcat(nome, "_get_pontilhado_ordenado.png"));
end

%Função que converte a imagem na escala de cinza para um pontilhado de forma ordenada com matriz de Bayer
function imagem_pontilhada = get_pontilhado_ordenado_Bayer(imagem_entrada, nome)
	
mascara = [0,12,3,15; 8,4,11,7; 2,14,1,13; 10,6,9,5];

	%Normaliza a imagem com valores de 0 a 15
	imagem_normalizada = double(imagem_entrada) / 255 * 15;

	imagem_pontilhada = zeros(4*size(imagem_entrada));

	for i = 1 : size(imagem_entrada, 1)
		for j = 1 : size(imagem_entrada, 2)
			imagem_pontilhada(4*(i-1)+1:4*i, 4*(j-1)+1:4*j) = imagem_normalizada(i,j) > mascara;
		end
	end
	imwrite(logical(imagem_pontilhada), strcat(nome,"_get_pontilhado_ordenado_Bayer.pbm"));
	imwrite(imagem_pontilhada, strcat(nome,"_get_pontilhado_ordenado_Bayer.png"));
end

%Função que converte a imagem na escala de cinza para um pontilhado com difusão de erro da forma Floyd Steinberg
function imagem_pontilhada = get_pontilhado_floyd_steinberg_varredura_normal(imagem_entrada, nome)
	
	tamanho_x = size(imagem_entrada, 1);
	tamanho_y = size(imagem_entrada, 2);

	copia_imagem = double(imagem_entrada);
	imagem_pontilhada = zeros(size(imagem_entrada));

	for i = 2 : tamanho_x-1
		for j = 1 : tamanho_y-1
			if (copia_imagem(i,j) > 128)
				imagem_pontilhada(i,j) = 1;
			else 
				imagem_pontilhada(i,j) = 0;
			end

			%calcula erro
			erro = copia_imagem(i,j) - imagem_pontilhada(i,j)*255;

			%distribui o erro
			copia_imagem(i+1, j  ) += 7/16*erro;
			copia_imagem(i-1, j+1) += 3/16*erro;
			copia_imagem(i  , j+1) += 5/16*erro;
			copia_imagem(i+1, j+1) += 1/16*erro;
		end
	end

	imwrite(logical(imagem_pontilhada), strcat(nome, "_get_pontilhado_floyd_steinberg_varredura_normal.pbm"));
	imwrite(imagem_pontilhada, strcat(nome, "_get_pontilhado_floyd_steinberg_varredura_normal.png"));
end

%Função que converte a imagem na escala de cinza para um pontilhado com difusão de erro da forma Floyd Steinberg
function imagem_pontilhada = get_pontilhado_floyd_steinberg_varredura_alternada(imagem_entrada, nome)
	
	tamanho_x = size(imagem_entrada, 1);
	tamanho_y = size(imagem_entrada, 2);

	copia_imagem = double(imagem_entrada);
	imagem_pontilhada = zeros(size(imagem_entrada));

	for i = 2 : tamanho_x-1

		inicio = [1, tamanho_y-1]((mod(i, 2) == 0)+ 1);
		fim  = [tamanho_y-1 , 1]((mod(i, 2) == 0)+ 1);
		passo    = [1, -1]((mod(i, 2) == 0)+ 1);

		for j = inicio : passo: fim
			if (copia_imagem(i,j) > 128)
				imagem_pontilhada(i,j) = 1;
			else 
				imagem_pontilhada(i,j) = 0;
			end

			%calcula erro
			erro = copia_imagem(i,j) - imagem_pontilhada(i,j)*255;

			%distribui o erro
			copia_imagem(i+passo, j  ) += 7/16*erro;
			copia_imagem(i-passo, j+1) += 3/16*erro;
			copia_imagem(i  , j+1) += 5/16*erro;
			copia_imagem(i+passo, j+1) += 1/16*erro;
		end
	end

	imwrite(logical(imagem_pontilhada), strcat(nome, "_get_pontilhado_floyd_steinberg_varredura_alternada.pbm"));
	imwrite(imagem_pontilhada, strcat(nome, "_get_pontilhado_floyd_steinberg_varredura_alternada.png"));
end


function rodar_todas_funcoes_para_todas_imagens()
	srcFiles = dir('/home/cc2013/ra145979/unicamp/mc940/laboratorio3/images/*.pgm');  % the folder in which ur images exists
	
	for i = 1 : length(srcFiles)
	    filename = strcat('/home/cc2013/ra145979/unicamp/mc940/laboratorio3/images/',srcFiles(i).name);
		[pathstr,nome,ext] = fileparts(filename);
		imagem_entrada = imread(filename);
	    get_pontilhado_ordenado(imagem_entrada, nome);
	    get_pontilhado_ordenado_Bayer(imagem_entrada, nome);
	    get_pontilhado_floyd_steinberg_varredura_normal(imagem_entrada, nome);
	    get_pontilhado_floyd_steinberg_varredura_alternada(imagem_entrada, nome);
	end

end
