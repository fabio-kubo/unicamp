import numpy as np
import math as math
from scipy.signal import argrelmin
from scipy.signal import argrelmax

def get_area(data_wavelet):

	area = []
	for item in data_wavelet:
		area.append(np.mean(np.abs(item)))

	return area

def get_normalizedDecay(data_wavelet):

	normalizedDecay = []

	for item in data_wavelet:
		sumAux = 0
		for i in range(0, len(item)-1):
			sumAux+= 0.5 if (item[i+1]-item[i]) < 0 else -0.5
		normalizedDecay.append(np.abs(sumAux/(len(item)-1)))

	return normalizedDecay

def get_lineLength(data_wavelet):
	lineLength = []

	for item in data_wavelet:
		sumAux = 0
		for i in range(1, len(item)):
			sumAux+= abs(item[i] - item[i-1])
		lineLength.append(sumAux)

	return lineLength

def get_meanEnergy(data_wavelet):
	meanEnergy = []

	for item in data_wavelet:
		meanEnergy.append(np.mean(np.power(item, 2)))

	return meanEnergy;

def get_averagePeakAmplitude(data_wavelet):
	peakAmplitude = []

	for item in data_wavelet:
		peaks = item[argrelmax(item)[0]]
		peakAmplitude.append(math.log10(np.mean(np.power(peaks, 2))))

	return peakAmplitude

def get_averageValleyAmplitude(data_wavelet):
	averageValleyAmplitude = []

	for item in data_wavelet:
		valleys = item[argrelmin(item)[0]]
		averageValleyAmplitude.append(math.log10(np.mean(np.power(valleys, 2))))

	return averageValleyAmplitude

def get_normalizedPeakNumber(data_wavelet):
	normalizedPeakNumber = []

	for item in data_wavelet:
		differences =[]
		nr_peaks = len(argrelmax(item)[0])
		for i in range(0, len(item)-1):
			differences.append(abs(item[i+1]-item[i]))

		normalizedPeakNumber.append(math.pow(np.mean(differences),-1)*nr_peaks)

	return normalizedPeakNumber

def get_peakVariation(data_wavelet):
	peakVariation = []
	variationIndice = []
	variationValue = []

	for item in data_wavelet:
		peaksIndices = argrelmax(item)[0]
		valleysIndices = argrelmin(item)[0]
		variationIndice = [abs(p-v) for p, v in zip(peaksIndices, valleysIndices)]

		peaksValues = item[argrelmax(item)[0]]
		valleysValues = item[argrelmin(item)[0]]
		variationValue = [abs(p-v) for p, v in zip(peaksValues, valleysValues)]

		peakVariation.append(1/(np.std(variationIndice)* np.std(variationValue)))

	return peakVariation

def get_rootMeanSquare(data_wavelet):
	rootMeanSquare = []

	for item in data_wavelet:
		rootMeanSquare.append(math.sqrt(np.mean(np.power(item, 2))))

	return rootMeanSquare

#TODO: Duvida... isso nao eh semelhante ao mean energy?
def get_waveletEnergy(data_wavelet):
	waveletEnergy = []

	#getting only details values
	for item in data_wavelet[1:]:
		waveletEnergy.append(np.mean(np.power(item, 2)))

	return waveletEnergy


def get_standartVariation(data_wavelet):
	standartVariation = []

	for item in data_wavelet:
		standartVariation.append(np.std(item))

	return standartVariation
