import java.io.Serializable;

public class Resposta  implements Serializable{
	
	private static final long serialVersionUID = 1L;
	private int idQuestao;
	private int qtdAlternativas;
	private String resposta;

	public Resposta(int id, int qtd, String resp){
		this.idQuestao = id;
		this.qtdAlternativas = qtd;
		this.resposta = resp.toUpperCase();
	}

	public Resposta(String resposta){

		String [] partes = resposta.split(";");

		this.idQuestao = Integer.parseInt(partes[0]);
		this.qtdAlternativas = Integer.parseInt(partes[1]);
		this.resposta = partes[2].toUpperCase();
	}

	public int getIdQuestao(){
		return this.idQuestao;
	}

	public int getQtdAlternativas(){
		return this.qtdAlternativas;
	}

	public String getResposta(){
		return this.resposta;
	}

	public void setIdQuestao(int novoId){
		this.idQuestao = novoId;
	}

	public void setQtdAlternativas(int novaQtd){
		this.qtdAlternativas = novaQtd;
	}

	public void setResposta(String novaResposta){
		this.resposta = novaResposta;
	}

	public String toString(){
		return getIdQuestao() + ";" + getQtdAlternativas() + ";" + getResposta();
	}
}