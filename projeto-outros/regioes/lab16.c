/*
	Laboratorio 16
	Regioes

	Esse programa realiza a alteracao de cor de uma regiao da imagem. O algoritmo implementado utiliza recursao,
	sendo que antes de tentar pintar o vizinho, confere-se se o mesmo existe. Caso o vizinho a ser pintado nao 
	seja da regiao, ele da um return, nao alterando a sua cor e nem chamando o altera_cor pros seus vizinhos.
	
	Aluno: Gabriel Seixas Disselli
	Ra: 146165
*/
#include <stdio.h>
#include <stdlib.h>

//metodo que imprime o resultado
void imprimir_resultado(int imagem[200][200], int largura, int altura){

	int i, j;

	printf("P2\n");
	printf("%d %d\n", largura, altura);
	printf("255\n");

	for (i = 0; i < altura; i++){
		for (j = 0; j < largura; j++){
			printf("%d ", imagem[i][j]);
		}
		printf("\n");
	}
}

//metodo que verifica se o indice esta no intervalo correto
int eh_indice_valido(int indice, int indiceMaximo){

	int valido;

	valido = 1;
	
	if(indice < 0){
		valido = 0;
	}
	else if(indice >= indiceMaximo){
		valido = 0;
	}

	return valido;
}

//metodo que altera recursivamente a cor de uma regiao
void alterar_cor(int imagem[200][200], int largura, int altura, int coordenada_y, int coordenada_x, int nova_cor, int cor_antiga){
	
	//se a cor for diferente da cor antiga, para a recursao
	if(imagem[coordenada_y][coordenada_x] != cor_antiga){
		return;
	}

	//altera a cor do pixel atual	
	imagem[coordenada_y][coordenada_x] = nova_cor;

	//altera as cores do vizinho
	//diagonal esquerda cima
	if(eh_indice_valido(coordenada_y-1, altura) && eh_indice_valido(coordenada_x-1, largura)){
		alterar_cor(imagem, largura, altura, coordenada_y-1, coordenada_x-1, nova_cor, cor_antiga);
	}
	//cima
	if(eh_indice_valido(coordenada_y-1, altura) && eh_indice_valido(coordenada_x, largura)){
		alterar_cor(imagem, largura, altura, coordenada_y-1, coordenada_x, nova_cor, cor_antiga);
	}
	//diagonal direita cima
	if(eh_indice_valido(coordenada_y-1, altura) && eh_indice_valido(coordenada_x+1, largura)){
		alterar_cor(imagem, largura, altura, coordenada_y-1, coordenada_x+1, nova_cor, cor_antiga);
	}
	//esquerda
	if(eh_indice_valido(coordenada_y, altura) && eh_indice_valido(coordenada_x-1, largura)){
		alterar_cor(imagem, largura, altura, coordenada_y, coordenada_x-1, nova_cor, cor_antiga);
	}
	//direita
	if(eh_indice_valido(coordenada_y, altura) && eh_indice_valido(coordenada_x+1, largura)){
		alterar_cor(imagem, largura, altura, coordenada_y, coordenada_x+1, nova_cor, cor_antiga);
	}
	//diagonal esquerda baixo
	if(eh_indice_valido(coordenada_y+1, altura) && eh_indice_valido(coordenada_x-1, largura)){
		alterar_cor(imagem, largura, altura, coordenada_y+1, coordenada_x-1, nova_cor, cor_antiga);
	}
	//baixo
	if(eh_indice_valido(coordenada_y+1, altura) && eh_indice_valido(coordenada_x, largura)){
		alterar_cor(imagem, largura, altura, coordenada_y+1, coordenada_x, nova_cor, cor_antiga);
	}
	//diagonal direita baixo
	if(eh_indice_valido(coordenada_y+1, altura) && eh_indice_valido(coordenada_x+1, largura)){
		alterar_cor(imagem, largura, altura, coordenada_y+1, coordenada_x+1, nova_cor, cor_antiga);
	}
}


int main(int argc, char *argv[]){

	int imagem[200][200], largura, altura, valorMaximo, coordenada_x, coordenada_y, nova_cor, i, j;
	char aux[10];
	FILE * arquivoImagem, * arquivoParametros;

	//verifica se esta sendo passado os dois parametros 
	if(argc <=2){
		printf("Esta faltando um parametro.\n");
		exit(EXIT_FAILURE);
	}	

	//le os arquivos de entrada
	arquivoImagem = fopen(argv[1],"r");
	arquivoParametros = fopen(argv[2],"r");

	if(!arquivoImagem || !arquivoParametros){
		printf("Erro ao abrir o arquivo.\n");
		exit(EXIT_FAILURE);
	}

	//descarta a primeira linha
	fscanf(arquivoImagem, "%s\n", aux);

	//le as dimensoes
	fscanf(arquivoImagem, "%d %d\n", &largura, &altura);

	//le o valor maximo (n vai precisar usar, pois o maior valor vai ser sempre 255)
	fscanf(arquivoImagem, "%d\n", &valorMaximo);

	//preenche a matriz
	for (i = 0; i < altura; i++){
		for (j = 0; j < largura; j++){
			fscanf(arquivoImagem, "%d ", &imagem[i][j]);
		}
	}

	//le as quantidades de colunas e linhas
	fscanf(arquivoParametros, "%d\n", &coordenada_y);
	fscanf(arquivoParametros, "%d\n", &coordenada_x);
	fscanf(arquivoParametros, "%d\n", &nova_cor);

	//altera a cor
	alterar_cor(imagem, largura, altura, coordenada_y, coordenada_x, nova_cor, imagem[coordenada_y][coordenada_x]);
	
	//imprime o resultado
	imprimir_resultado(imagem, largura, altura);

	return 0;
}