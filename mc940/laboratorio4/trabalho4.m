%Fabio Tadashi Kaneiwa Kubo RA: 145979

%Metodo que classifica os pixels da imagem em objeto ou fundo de acordo com um limiar T=128
function imagem_processada = metodo_global(imagem_entrada, nome)
    imagem_processada = zeros(size(imagem_entrada));
    imagem_processada(imagem_entrada > 128) = 1;
    
    %gera o histograma
    nomeHistograma = strcat(nome, "_histogram_metodo_global");
    figure, imhist(imagem_processada);
    print("-dpng", nomeHistograma)

    %exibe metricas
    tamanho = size(imagem_processada, 1) * size(imagem_processada, 2);
    quantidade_preto = sum(sum(imagem_processada == 0));
    disp(nome)
    porcentagem = quantidade_preto/tamanho
    
    %salva imagem
    imwrite(logical(imagem_processada), strcat(nome, "_metodo_global.pbm"));
    imwrite(imagem_processada, strcat(nome, "_metodo_global.png"));
end

%Metodo que classifica os pixels da imagem em objeto ou fundo
function imagem_processada = metodo_bernsen(imagem_entrada, nome)

    dimensao_n = 3; %apenas numeros impares
    deslocamento = (dimensao_n-1) / 2;

    inicio = 1 + deslocamento;
    i_final = size(imagem_entrada, 1)-deslocamento;
    j_final = size(imagem_entrada, 2)-deslocamento;

    imagem_processada = zeros(size(imagem_entrada));
    
    for i = inicio : i_final
        for j = inicio : j_final

            vizinhanca_matriz = imagem_entrada(i-deslocamento:i+deslocamento, j-deslocamento:j+deslocamento);
            vizinhanca_vetor = vizinhanca_matriz(:);

            limiar = (min(vizinhanca_vetor) + max(vizinhanca_vetor))/2;

            if(imagem_entrada(i,j) > limiar)
                imagem_processada(i,j) = 1;
            end
        end
    end

    %gera o histograma
    nomeHistograma = strcat(nome, "_histogram_metodo_bernsen");
    figure, imhist(imagem_processada);
    print("-dpng", nomeHistograma)

    %exibe metricas
    tamanho = size(imagem_processada, 1) * size(imagem_processada, 2);
    quantidade_preto = sum(sum(imagem_processada == 0));
    disp(nome)
    porcentagem = quantidade_preto/tamanho

    %salva imagem
    imwrite(logical(imagem_processada), strcat(nome, "_metodo_bernsen.pbm"));
    imwrite(imagem_processada, strcat(nome, "_metodo_bernsen.png"));
end

%Metodo que classifica os pixels da imagem em objeto ou fundo
function imagem_processada = metodo_niblack(imagem_entrada, nome)

    dimensao_n = 9; %apenas numeros impares
    constante_k = 0.005;
    deslocamento = (dimensao_n-1) / 2;

    inicio = 1 + deslocamento;
    i_final = size(imagem_entrada, 1)-deslocamento;
    j_final = size(imagem_entrada, 2)-deslocamento;

    imagem_processada = zeros(size(imagem_entrada));
    
    for i = inicio : i_final
        for j = inicio : j_final
            vizinhanca = imagem_entrada(i-deslocamento:i+deslocamento, j-deslocamento:j+deslocamento);
            media = mean2(vizinhanca);
            desvio_padrao = std2(vizinhanca);
            limiar = media + constante_k*desvio_padrao;

            if(imagem_entrada(i,j) > limiar)
                imagem_processada(i,j) = 1;
            end
        end
    end

    %gera o histograma
    nomeHistograma = strcat(nome, "_histogram_metodo_niblack");
    figure, imhist(imagem_processada);
    print("-dpng", nomeHistograma)

    %exibe metricas
    tamanho = size(imagem_processada, 1) * size(imagem_processada, 2);
    quantidade_preto = sum(sum(imagem_processada == 0));
    disp(nome)
    porcentagem = quantidade_preto/tamanho

    %salva imagem
    imwrite(logical(imagem_processada), strcat(nome, "_metodo_niblack.pbm"));
    imwrite(imagem_processada, strcat(nome, "_metodo_niblack.png"));
end

%Metodo que classifica os pixels da imagem em objeto ou fundo
function imagem_processada = metodo_sauvola_pietaksinen(imagem_entrada, nome)

    dimensao_n = 5; %apenas numeros impares
    constante_k = 0.04;
    constante_r = 30;

    deslocamento = (dimensao_n-1) / 2;

    inicio = 1 + deslocamento;
    i_final = size(imagem_entrada, 1)-deslocamento;
    j_final = size(imagem_entrada, 2)-deslocamento;

    imagem_processada = zeros(size(imagem_entrada));
    
    for i = inicio : i_final
        for j = inicio : j_final

            vizinhanca = imagem_entrada(i-deslocamento:i+deslocamento, j-deslocamento:j+deslocamento);

            media = mean2(vizinhanca);
            desvio_padrao = std2(vizinhanca);

            limiar = media * (1 + constante_k*(desvio_padrao/constante_r -1));

            if(imagem_entrada(i,j) > limiar)
                imagem_processada(i,j) = 1;
            end
        end
    end

    %exibe metricas
    tamanho = size(imagem_processada, 1) * size(imagem_processada, 2);
    quantidade_preto = sum(sum(imagem_processada == 0));
    disp(nome)
    porcentagem = quantidade_preto/tamanho

    %gera o histograma
    nomeHistograma = strcat(nome, '_histogram_metodo_sauvola_pietaksinen');
    figure, imhist(imagem_processada);
    print("-dpng", nomeHistograma)

    %salva imagem
    imwrite(logical(imagem_processada), strcat(nome, "_metodo_sauvola_pietaksinen.pbm"));
    imwrite(imagem_processada, strcat(nome, "_metodo_sauvola_pietaksinen.png"));
end

function imagem_processada = metodo_phansalskar_more_sabale(imagem_entrada, nome)
    
    dimensao_n = 7; %apenas numeros impares
    constante_k = 0.05;
    constante_r = 15;
    constante_p = 2;
    constante_q = 10;

    deslocamento = (dimensao_n-1) / 2

    inicio = 1 + deslocamento;
    i_final = size(imagem_entrada, 1)-deslocamento;
    j_final = size(imagem_entrada, 2)-deslocamento;

    imagem_processada = zeros(size(imagem_entrada));
    
    for i = inicio : i_final
        for j = inicio : j_final

            vizinhanca = imagem_entrada(i-deslocamento:i+deslocamento, j-deslocamento:j+deslocamento);

            media = mean2(vizinhanca);
            desvio_padrao = std2(vizinhanca);

            limiar = media* (1 + constante_p*exp(-constante_q*media) + constante_k*(desvio_padrao/constante_r-1) );

            if(imagem_entrada(i,j) > limiar)
                imagem_processada(i,j) = 1;
            end
        end
    end

    %gera o histograma
    nomeHistograma = strcat(nome, "_histogram_metodo_phansalskar_more_sabale");
    figure, imhist(imagem_processada);
    print("-dpng", nomeHistograma)

    %exibe metricas
    tamanho = size(imagem_processada, 1) * size(imagem_processada, 2);
    quantidade_preto = sum(sum(imagem_processada == 0));
    disp(nome)
    porcentagem = quantidade_preto/tamanho

    %salva imagem
    imwrite(logical(imagem_processada), strcat(nome, "_metodo_phansalskar_more_sabale.pbm"));
    imwrite(imagem_processada, strcat(nome, "_metodo_phansalskar_more_sabale.png"));
end

function imagem_processada = metodo_contraste(imagem_entrada, nome)
    
    dimensao = 5; %apenas numeros impares
    deslocamento = (dimensao - 1)/2;

    inicio = 1 + deslocamento;
    i_final = size(imagem_entrada, 1)-deslocamento;
    j_final = size(imagem_entrada, 2)-deslocamento;

    imagem_processada = zeros(size(imagem_entrada));
    
    for i = inicio : i_final
        for j = inicio : j_final

            vizinhanca_matriz = imagem_entrada(i-deslocamento:i+deslocamento, j-deslocamento:j+deslocamento);
            vizinhanca_vetor = vizinhanca_matriz(:);

            distancia_maior_valor = max(vizinhanca_vetor) - imagem_entrada(i,j);
            distancia_menor_valor = imagem_entrada(i,j) - min(vizinhanca_vetor);

            if(distancia_maior_valor > distancia_menor_valor)
                imagem_processada(i,j) = 1;
            end
        end
    end

    %gera o histograma
    nomeHistograma = strcat(nome, "_histogram_metodo_contraste");
    figure, imhist(imagem_processada);
    print("-dpng", nomeHistograma)

    %exibe metricas
    tamanho = size(imagem_processada, 1) * size(imagem_processada, 2);
    quantidade_preto = sum(sum(imagem_processada == 0));
    disp(nome)
    porcentagem = quantidade_preto/tamanho

    %salva imagem
    imwrite(logical(imagem_processada), strcat(nome, "_metodo_contraste.pbm"));
    imwrite(imagem_processada, strcat(nome, "_metodo_contraste.png"));
end

function imagem_entrada = metodo_media(imagem_entrada, nome)
    
    dimensao = 5; %apenas numeros impares
    deslocamento = (dimensao - 1)/2;
    
    inicio = 1 + deslocamento;
    i_final = size(imagem_entrada, 1)-deslocamento;
    j_final = size(imagem_entrada, 2)-deslocamento;

    imagem_processada = zeros(size(imagem_entrada));
    
    for i = inicio : i_final
        for j = inicio : j_final
            limiar = mean(imagem_entrada(i-deslocamento:i+deslocamento, j-deslocamento:j+deslocamento)(:));
            if(imagem_entrada(i,j) > limiar)
                imagem_processada(i,j) = 1;
            end
        end
    end

    %gera o histograma
    nomeHistograma = strcat(nome, "_histogram_metodo_media");
    figure, imhist(imagem_processada);
    print("-dpng", nomeHistograma)

    %exibe metricas
    tamanho = size(imagem_processada, 1) * size(imagem_processada, 2);
    quantidade_preto = sum(sum(imagem_processada == 0));
    disp(nome)
    porcentagem = quantidade_preto/tamanho

    %salva imagem
    imwrite(logical(imagem_processada), strcat(nome, "_metodo_media.pbm"));
    imwrite(imagem_processada, strcat(nome, "_metodo_media.png"));
end

function imagem_processada = metodo_mediana(imagem_entrada, nome)
    
    dimensao = 5; %apenas numeros impares
    deslocamento = (dimensao - 1)/2;

    inicio = 1 + deslocamento;
    i_final = size(imagem_entrada, 1)-deslocamento;
    j_final = size(imagem_entrada, 2)-deslocamento;

    imagem_processada = zeros(size(imagem_entrada));
    
    for i = inicio : i_final
        for j = inicio : j_final
            vizinhanca_matriz = imagem_entrada(i-deslocamento:i+deslocamento, j-deslocamento:j+deslocamento);
            vizinhanca_vetor = vizinhanca_matriz(:);
            
            limiar = median(vizinhanca_vetor);
            if(imagem_entrada(i,j) > limiar)
                imagem_processada(i,j) = 1;
            end
        end
    end

    %gera o histograma
    nomeHistograma = strcat(nome, "_histogram_metodo_mediana");
    figure, imhist(imagem_processada);
    print("-dpng", nomeHistograma)

    %exibe metricas
    tamanho = size(imagem_processada, 1) * size(imagem_processada, 2);
    quantidade_preto = sum(sum(imagem_processada == 0));
    disp(nome)
    porcentagem = quantidade_preto/tamanho

    %salva imagem
    imwrite(logical(imagem_processada), strcat(nome, "_metodo_mediana.pbm"));
    imwrite(imagem_processada, strcat(nome, "_metodo_mediana.png"));
end

function rodar_todas_funcoes_para_todas_imagens()
    srcFiles = dir('/home/cc2013/ra145979/unicamp/mc940/laboratorio4/images/*.pgm');  % the folder in which ur images exists
    
    for i = 1 : length(srcFiles)
        filename = strcat('/home/cc2013/ra145979/unicamp/mc940/laboratorio4/images/',srcFiles(i).name);
        [pathstr,nome,ext] = fileparts(filename);
        imagem_entrada = imread(filename);
        
        metodo_phansalskar_more_sabale(imagem_entrada, nome);

        metodo_global(imagem_entrada, nome)
        metodo_bernsen(imagem_entrada, nome)
        metodo_niblack(imagem_entrada, nome)
        metodo_sauvola_pietaksinen(imagem_entrada, nome)
        metodo_phansalskar_more_sabale(imagem_entrada, nome)
        metodo_contraste(imagem_entrada, nome)
        metodo_media(imagem_entrada, nome)
        metodo_mediana(imagem_entrada, nome)
    end

end