
imgOriginal = imread('../pgm-examples/baboon.pgm');
img1 = imgOriginal;
img2 = imgOriginal;
img3 = imgOriginal;
img4 = imgOriginal;
img5 = imgOriginal;
img6 = imgOriginal;
img7 = imgOriginal;


%exE
img5([1:end end:-1:1], :) = img5([end:-1:1 1:end], :);
imwrite(img5, 'saida5.pgm');

exA
img1(img1 > 127) = 255;
img1(img1 < 128) = 0;

%exB
img2 = 255 - img2;
imshow(img1);

%exC
img3 = power(img3/255, exp(0.5))*255;
imwrite(img3, 'saida3.pgm');

%exD
img4 = power(img3/255, exp(-0.5))*255;
imwrite(img4, 'saida4.pgm');

%F
aux = img6(1:2:end,:);  % odd rows

img6(1:2:end,:) = img6(2:2:end,:); % even rows
img6(2:2:end,:) = aux;
imwrite(img6, 'saida6.pgm');

%G
aux = img7(:,1:2:end);  % odd columns
img7(:,1:2:end) = img7(:,2:2:end); % even columns
img7(:,2:2:end) = aux;
imwrite(img6, 'saida7.pgm');

