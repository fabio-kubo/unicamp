/*
O objetivo desse programa eh implementar a busca por profundidade
e extrair algumas caracteristicas/propriedades do grafo.
*/

#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>

typedef struct Elemento {
    int vertice;
    struct Elemento* proximo;
} Elemento;

typedef struct Lista {
    int tamanho;
    Elemento* inicio;
} Lista;

/*
  Inicializa os campos da estrutura Lista.  Esta funcao deve ser
  chamada antes que qualquer elemento seja inserido. Complexidade:
  constante.
  Parametros:
  lista: apontador para a lista
*/
void inicializa(Lista* lista);

/*
  Insere um elemento no inicio da lista. Complexidade: constante.
  Parametros:
  lista: apontador para a lista
  vertice: inteiro representando o vertice a ser inserido
*/
void insere(Lista* lista, int vertice);

/*
  Remove todos os elementos da lista. Complexidade: linear no tamanho
  da lista.
  Parametros:
  lista: apontador para a lista
*/
void esvazia(Lista* lista);

Lista * mock_grafo(int *qtd_vertices);

void dfs(Lista * lista, int qtd_vertices, int * d, int * f, int * ordem);

void dfs_visit(Lista * lista_adjacencias, int id_vertice, int * tempo, int * cor, int * predecessor, int * d, int * f);

void imprimir_grafo(Lista* lista, int qtd_vertices);

void imprimir_tempos(int qtd_vertices, int * d, int * f);

Lista * get_transposta(Lista * original, int qtd_vertices);

void calculaOrdem(int * f, int qtd_vertices, int * ordem);

int main() {

  Lista * lista_adjacencias, * lista_transposta;
  int * d, * f, i, * ordem;
  int qtd_vertices;

  lista_adjacencias = mock_grafo(&qtd_vertices);
  lista_transposta = get_transposta(lista_adjacencias, qtd_vertices);

  d = (int *) malloc(sizeof(int)*qtd_vertices);
  f = (int *) malloc(sizeof(int)*qtd_vertices);
  ordem = (int *) malloc(sizeof(int)*qtd_vertices);

  for (i = 0; i < qtd_vertices; i++){
    d[i] = i;
  }

  dfs(lista_adjacencias, qtd_vertices, d, f, ordem);

  imprimir_grafo(lista_adjacencias, qtd_vertices);
  imprimir_tempos(qtd_vertices, d, f);
  
  imprimir_grafo(lista_transposta, qtd_vertices);

  calculaOrdem(f, qtd_vertices, ordem);
  dfs(lista_transposta, qtd_vertices, d, f, ordem);

  for (i = 0; i < qtd_vertices; i++){
    esvazia(&lista_transposta[i]);
    esvazia(&lista_adjacencias[i]);
  }

  free(lista_transposta);
  free(lista_adjacencias);
  free(d);
  free(f);

  return 0;
}


Lista * get_transposta(Lista * original, int qtd_vertices){
  Lista * transposta;
  Elemento * atual;
  int i;

  transposta = (Lista *) malloc(sizeof(Lista) * qtd_vertices);

  for (i = 0; i < qtd_vertices; i++){
      inicializa(&transposta[i]);
  }

  for (i = 0; i < qtd_vertices; i++){
    atual = original[i].inicio;
    while(atual != NULL){
      insere(&transposta[atual->vertice], i);
      atual = atual->proximo;
    }
  }

  return transposta;
}

Lista * mock_grafo(int * qtd_vertices){
  Lista * lista_adjacencias;

  (* qtd_vertices) = 8;
  lista_adjacencias = (Lista *) malloc((* qtd_vertices)*sizeof(Lista));

  Elemento * aux;

  //a == 0
  inicializa(&lista_adjacencias[0]);
  insere(&lista_adjacencias[0], 1);

  //b == 1
  inicializa(&lista_adjacencias[1]);
  insere(&lista_adjacencias[1],2);
  insere(&lista_adjacencias[1],4);
  insere(&lista_adjacencias[1],5);

  //c == 2
  inicializa(&lista_adjacencias[2]);
  insere(&lista_adjacencias[2],3);
  insere(&lista_adjacencias[2],6);

  //d == 3
  inicializa(&lista_adjacencias[3]);
  insere(&lista_adjacencias[3],2);
  insere(&lista_adjacencias[3],7);

  //e == 4
  inicializa(&lista_adjacencias[4]);
  insere(&lista_adjacencias[4],5);
  insere(&lista_adjacencias[4],0);

  //f == 5
  inicializa(&lista_adjacencias[5]);
  insere(&lista_adjacencias[5],6);
  

  //g == 6
  inicializa(&lista_adjacencias[6]);
  insere(&lista_adjacencias[6],5);
  insere(&lista_adjacencias[6],7);

  //h == 7
  inicializa(&lista_adjacencias[7]);
  insere(&lista_adjacencias[7], 7);


  return lista_adjacencias;
}

void dfs(Lista * lista, int qtd_vertices, int * d, int * f, int * ordem){
  int tempo, i, *cor, *predecessor;

  cor = (int *) malloc(sizeof(int)*qtd_vertices);
  predecessor = (int *) malloc(sizeof(int)*qtd_vertices);

  for (i = 0; i < qtd_vertices; i++){
    cor[i] = 0; // branco
    predecessor[i] = -1; // null
    d[i] = 0;
    f[i] = 0;
  }
  tempo = 0;

  for (i = 0; i < qtd_vertices; i++){
    if(cor[ordem[i]] == 0){
      printf("Componente: %c\n", ordem[i] + 97);
      dfs_visit(lista, ordem[i], &tempo, cor, predecessor, d, f);
    }
  }
}

void dfs_visit(Lista * lista_adjacencias, int id_vertice, int * tempo, int * cor, int * predecessor, int * d, int * f){
  
  cor[id_vertice] = 1; //cinza
  (* tempo) = (* tempo) + 1;
  d[id_vertice] = (* tempo);

  Elemento * adj = lista_adjacencias[id_vertice].inicio;
  while(adj != NULL){
    if(cor[adj->vertice] == 0){
      predecessor[adj->vertice] = id_vertice;
      dfs_visit(lista_adjacencias, adj->vertice, tempo, cor, predecessor, d, f);
    }
    adj = adj->proximo;
  }
  cor[id_vertice] = 2;
  (* tempo) = (* tempo) + 1;
  f[id_vertice] = (* tempo);
}

void imprimir_grafo(Lista* lista, int qtd_vertices){
  int i;

  for (i = 0; i < qtd_vertices; i++){
    printf("Adjacentes de %c:", i + 97);
      /* Percorre lista */
      Elemento* atual = lista[i].inicio;
      while (atual != NULL) {
          printf(" %c", atual->vertice + 97);
          atual = atual->proximo;
      }
      printf("\n");
  }
}

void calculaOrdem(int * f, int qtd_vertices, int * ordem){

  int i, j, maior, i_maior;

  for (i = 0; i < qtd_vertices; i++){
    i_maior = 0;
    maior = f[0];
    for (j = 1; j < qtd_vertices; j++){
      if(maior < f[j]){
        i_maior = j;
        maior = f[j];
      }
    }

    f[i_maior] = 0; // retira da lista de f
    ordem[i] = i_maior;
  }
}

void imprimir_tempos(int qtd_vertices, int * d, int * f){
  int i;

  printf("Tempos de cada vertice:\n");
  for (i = 0; i < qtd_vertices; i++){
    printf("%c: %d %d\n", 97+i, d[i], f[i]);
  }
}

void inicializa(Lista* lista) {
    lista->tamanho = 0;
    lista->inicio = NULL;
}

void insere(Lista* lista, int vertice) {
    Elemento* novo = (Elemento*)malloc(sizeof(Elemento));
    novo->vertice = vertice;
    novo->proximo = lista->inicio;
    lista->tamanho++;
    lista->inicio = novo;
}

void esvazia(Lista* lista) {
    Elemento* atual = lista->inicio;
    while (atual != NULL) {
        Elemento* proximo = atual->proximo;
        free(atual);
        atual = proximo;
    }
    lista->tamanho = 0;
    lista->inicio = NULL;
}