import numpy as np
import pywt

def get_datasetA():
    #Le os sinais do Set SetA
    data_setA = []
    for i in range(1,101):
        arqComSinais = open("../amostras/SetA/Z{0}.txt".format(str(i).zfill(3)))
        data_setA.append(map(int, arqComSinais.readlines())) #converte para inteiro
    data_setA = np.array(data_setA)

    return data_setA

def get_datasetB():
    #Le os sinais do Set B
    data_setB = []
    for i in range(1,101):
        arqComSinais = open("../amostras/SetB/O{0}.txt".format(str(i).zfill(3)))
        data_setB.append(map(int, arqComSinais.readlines())) #converte para inteiro
    data_setB = np.array(data_setB)

    return data_setB

def get_datasetC():
    #Le os sinais do Set C
    data_setC = []
    for i in range(1,101):
        arqComSinais = open("../amostras/SetC/N{0}.txt".format(str(i).zfill(3)))
        data_setC.append(map(int, arqComSinais.readlines())) #converte para inteiro
    data_setC = np.array(data_setC)

    return data_setC

def get_datasetD():
    #Le os sinais do Set D
    data_setD = [] 
    for i in range(1,101):
        arqComSinais = open("../amostras/SetD/F{0}.txt".format(str(i).zfill(3)))
        data_setD.append(map(int, arqComSinais.readlines())) #converte para inteiro
    data_setD = np.array(data_setD)
    
    return data_setD

def get_datasetE():
    #Le os sinais do Set E
    data_setE = []
    for i in range(1,101):
        arqComSinais = open("../amostras/SetE/S{0}.txt".format(str(i).zfill(3)))
        data_setE.append(map(int, arqComSinais.readlines())) #converte para inteiro
    data_setE = np.array(data_setE)

    return data_setE

def get_data_wavelet_form():

    #healthy people
    datasetA = get_datasetA()
    datasetB = get_datasetB()

    #epileptic people
    datasetC = get_datasetC()
    datasetD = get_datasetD()
    datasetE = get_datasetE()

    #initialize list
    data_wavelet_A = [[],[],[],[],[],[]]
    data_wavelet_B = [[],[],[],[],[],[]]
    data_wavelet_C = [[],[],[],[],[],[]]
    data_wavelet_D = [[],[],[],[],[],[]]
    data_wavelet_E = [[],[],[],[],[],[]]

    #apply wavelet transform
    for data in datasetA:
        cA5, cD5, cD4, cD3, cD2, cD1 = pywt.wavedec(data, 'db4', level=5)
        data_wavelet_A[0] += [cA5]
        data_wavelet_A[1] += [cD1]
        data_wavelet_A[2] += [cD2]
        data_wavelet_A[3] += [cD3]
        data_wavelet_A[4] += [cD4]
        data_wavelet_A[5] += [cD5]

    for data in datasetB:
        cA5, cD5, cD4, cD3, cD2, cD1 = pywt.wavedec(data, 'db4', level=5)
        data_wavelet_B[0] += [cA5]
        data_wavelet_B[1] += [cD1]
        data_wavelet_B[2] += [cD2]
        data_wavelet_B[3] += [cD3]
        data_wavelet_B[4] += [cD4]
        data_wavelet_B[5] += [cD5]

    for data in datasetC:
        cA5, cD5, cD4, cD3, cD2, cD1 = pywt.wavedec(data, 'db4', level=5)
        data_wavelet_C[0] += [cA5]
        data_wavelet_C[1] += [cD1]
        data_wavelet_C[2] += [cD2]
        data_wavelet_C[3] += [cD3]
        data_wavelet_C[4] += [cD4]
        data_wavelet_C[5] += [cD5]

    for data in datasetD:
        cA5, cD5, cD4, cD3, cD2, cD1 = pywt.wavedec(data, 'db4', level=5)
        data_wavelet_D[0] += [cA5]
        data_wavelet_D[1] += [cD1]
        data_wavelet_D[2] += [cD2]
        data_wavelet_D[3] += [cD3]
        data_wavelet_D[4] += [cD4]
        data_wavelet_D[5] += [cD5]

    for data in datasetE:
        cA5, cD5, cD4, cD3, cD2, cD1 = pywt.wavedec(data, 'db4', level=5)
        data_wavelet_E[0] += [cA5]
        data_wavelet_E[1] += [cD1]
        data_wavelet_E[2] += [cD2]
        data_wavelet_E[3] += [cD3]
        data_wavelet_E[4] += [cD4]
        data_wavelet_E[5] += [cD5]

    return data_wavelet_A, data_wavelet_B, data_wavelet_C, data_wavelet_D, data_wavelet_E