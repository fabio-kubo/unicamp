import java.net.*;
import java.io.*;

public class Servidor {

	static final int NR_PORTA = 12666;
	private ServerSocket servidor;
	private Socket cliente;

	public Servidor(){

	}

	public void inicializar() throws Exception{

		System.out.println("Iniciando servidor para a porta: " + NR_PORTA +"...");
		this.servidor = new ServerSocket(NR_PORTA);
		System.out.println("Porta aberta com sucesso!");

	}

	public void conectarCliente() throws Exception{

		System.out.println("Esperando cliente...");
		this.cliente = servidor.accept();
		System.out.println("Cliente conectado!");
	}

	public void iniciarTransmissao() throws Exception{

		BufferedReader in = new BufferedReader(new InputStreamReader(cliente.getInputStream()));

		String inputLine;
		while ((inputLine = in.readLine()) != null) {
		        
			System.out.println(inputLine);
			if (inputLine.equals("exit"))
				break;
	    }
	}

	public void finalizar() throws Exception{

		this.cliente.close();
		this.servidor.close();
	}

	
	public static void main(String args[]) throws IOException {

		Servidor serv = new Servidor();

		try{

			//Inicializa o servidor
			serv.inicializar();

			//Conecta com cliente
			serv.conectarCliente();

			//recebe informações do cliente
			serv.iniciarTransmissao();

			//Da close nos sockets
			serv.finalizar();
		}
		catch(Exception e){
			System.out.println("Erro: " + e.getMessage());
		}

	}

}