import java.io.*;
import java.net.*;

class Cliente{
   public static void main(String args[]) throws Exception{

      BufferedReader bufferReader = new BufferedReader(new InputStreamReader(System.in));
      DatagramSocket clienteSocket = new DatagramSocket();
      byte[] mensagemBytes = new byte[1024];
      byte[] mensagemRecebidaBytes = new byte[1024];
      InetAddress IPAddress = InetAddress.getByName("localhost");
      

      //le a mensagem do cliente
      System.out.println("Entre com a mensagem que deseja inverter:");
      String sentence = bufferReader.readLine();
      
      //converte para bytes
      mensagemBytes = sentence.getBytes();

      //cria e envia o pacote
      DatagramPacket pacoteAEnviar = new DatagramPacket(mensagemBytes, mensagemBytes.length, IPAddress, 9876);
      clienteSocket.send(pacoteAEnviar);

      //recebe pacote do servidor
      DatagramPacket receivePacket = new DatagramPacket(mensagemRecebidaBytes, mensagemRecebidaBytes.length);
      clienteSocket.receive(receivePacket);

      //Exibe no console
      String mensagemServidor = new String(receivePacket.getData());
      System.out.println("Mensagem do server: " + mensagemServidor);
      clienteSocket.close();
   }
}