import DataReader
import Features
from sklearn.svm import SVC
from sklearn.ensemble import RandomForestClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.metrics import accuracy_score, f1_score, precision_score, recall_score


#Getting the EEG data
training_data = DataReader.get_training_data()
test_data = DataReader.get_test_data()


#Extracting features
training_features_matrix, training_labels = Features.get_features_matrix(training_data)
test_features_matrix, test_labels = Features.get_features_matrix(test_data)

#-------------------Training the svm---------------------------------------
#---------------------Classifiers---------------------

clf_svm = SVC(kernel="poly")
clf_svm.fit(training_features_matrix, training_labels)
labels_resultado_svm = clf_svm.predict(test_features_matrix)

accuracy_obtained = accuracy_score(test_labels, labels_resultado_svm)

print '------------------SVM------------------'
print 'Accuracy: {0}'.format(accuracy_obtained)
print 'f1_score: {0}'.format(f1_score(test_labels, labels_resultado_svm))
print 'Precision score: {0}'.format(precision_score(test_labels, labels_resultado_svm))
print 'Recall score: {0}'.format(recall_score(test_labels, labels_resultado_svm))


clf_rf = RandomForestClassifier()
clf_rf.fit(training_features_matrix, training_labels)
labels_resultado_rf = clf_rf.predict(test_features_matrix)

accuracy_obtained = accuracy_score(test_labels, labels_resultado_rf)

print '------------------RF------------------'
print 'Accuracy: {0}'.format(accuracy_obtained)
print 'f1_score: {0}'.format(f1_score(test_labels, labels_resultado_rf))
print 'Precision score: {0}'.format(precision_score(test_labels, labels_resultado_rf))
print 'Recall score: {0}'.format(recall_score(test_labels, labels_resultado_rf))


clf_knn = KNeighborsClassifier()
clf_knn.fit(training_features_matrix, training_labels)
labels_resultado_knn = clf_knn.predict(test_features_matrix)
	
accuracy_obtained = accuracy_score(test_labels, labels_resultado_knn)

print '------------------KNN------------------'
print 'Accuracy: {0}'.format(accuracy_obtained)
print 'f1_score: {0}'.format(f1_score(test_labels, labels_resultado_knn))
print 'Precision score: {0}'.format(precision_score(test_labels, labels_resultado_knn))
print 'Recall score: {0}'.format(recall_score(test_labels, labels_resultado_knn))