import java.io.*;
import java.net.*;

class Cliente{
   public static void main(String args[]) throws Exception{

      BufferedReader bufferReader = new BufferedReader(new InputStreamReader(System.in));
      DatagramSocket clienteSocket = new DatagramSocket();
      byte[] mensagemBytes;
      byte[] mensagemRecebidaBytes;
      InetAddress IPAddress = InetAddress.getByName("localhost");

      ByteArrayOutputStream outputStream;
      ObjectOutputStream objOutStream;
      

      for (; ; ) {

            mensagemBytes = new byte[1024];
            mensagemRecebidaBytes = new byte[1024];
            outputStream = new ByteArrayOutputStream();
            objOutStream = new ObjectOutputStream(outputStream);

            //le a mensagem do cliente e cria o objeto de resposta
            System.out.println("Entre com a resposta (Formato: <número da questão>;<número alternativas>;<respostas>):");
            String sentence = bufferReader.readLine();

            Resposta resposta = new Resposta(sentence);
            objOutStream.writeObject(resposta);
            
            //converte para bytes
            mensagemBytes = outputStream.toByteArray();

            //cria e envia o pacote
            DatagramPacket pacoteAEnviar = new DatagramPacket(mensagemBytes, mensagemBytes.length, IPAddress, 9876);
            clienteSocket.send(pacoteAEnviar);

            //recebe pacote do servidor
            DatagramPacket receivePacket = new DatagramPacket(mensagemRecebidaBytes, mensagemRecebidaBytes.length);
            clienteSocket.receive(receivePacket);

            ByteArrayInputStream in = new ByteArrayInputStream(receivePacket.getData());
            ObjectInputStream is = new ObjectInputStream(in);

            try {
                  Correcao correcao = (Correcao) is.readObject();
                  System.out.println(correcao.toString());
            } catch (ClassNotFoundException e) {
                  e.printStackTrace();
            }
            
      }
   }
}