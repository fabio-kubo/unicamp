#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>

typedef struct Elemento {
    int vertice;
    struct Elemento* proximo;
} Elemento;

typedef struct Lista {
    int tamanho;
    Elemento* inicio;
} Lista;

/*
  Inicializa os campos da estrutura Lista.  Esta funcao deve ser
  chamada antes que qualquer elemento seja inserido. Complexidade:
  constante.
  Parametros:
  lista: apontador para a lista
*/
void inicializa(Lista* lista);

/*
  Insere um elemento no inicio da lista. Complexidade: constante.
  Parametros:
  lista: apontador para a lista
  vertice: inteiro representando o vertice a ser inserido
*/
void insere(Lista* lista, int vertice);

/*
  Remove todos os elementos da lista. Complexidade: linear no tamanho
  da lista.
  Parametros:
  lista: apontador para a lista
*/
void esvazia(Lista* lista);

void criarGrafo(Lista* lista, int qtd_vertices);

void imprimir(Lista* lista, int qtd_vertices);

int main() {
  int qtd_vertices, i;
  Lista* lista;

  printf("Digite o numero de vertices desejado: ");
  scanf("%d", &qtd_vertices);

  lista = (Lista *) malloc(sizeof(Lista) * qtd_vertices);
  for (i = 0; i < qtd_vertices; i++){
    inicializa(&lista[i]);
  }
  
  criarGrafo(lista, qtd_vertices);
  imprimir(lista, qtd_vertices);

  for (i = 0; i < qtd_vertices; i++){
    esvazia(&lista[i]);
  }

  free(lista);
  return 0;
}

void criarGrafo(Lista* lista, int qtd_vertices){
  int i, j, qtd_divisores;

  //caso i = 0
  for (j = 1; j < qtd_vertices; j++){
    insere(lista, j);
  }
  lista[0].tamanho = qtd_vertices - 1;

  //caso 1 < i < qtd_vertices
  for (i = 1; i < qtd_vertices; i++){
    qtd_divisores = 0;
    for (j = 1; j < i; j++){
      if(i % j == 0){
        insere(&lista[i], j);
        qtd_divisores++;
      }
    }
    lista[i].tamanho = qtd_divisores;
  }
}

void imprimir(Lista* lista, int qtd_vertices){
  int i;

  for (i = 0; i < qtd_vertices; i++){
    printf("Imprimindo vertices adjacentes de %d:\n", i);
      /* Percorre lista */
      Elemento* atual = lista[i].inicio;
      printf("Elementos:");
      while (atual != NULL) {
          printf(" %d", atual->vertice);
          atual = atual->proximo;
      }
      printf("\n");
  }
}

void inicializa(Lista* lista) {
    lista->tamanho = 0;
    lista->inicio = NULL;
}

void insere(Lista* lista, int vertice) {
    Elemento* novo = (Elemento*)malloc(sizeof(Elemento));
    novo->vertice = vertice;
    novo->proximo = lista->inicio;
    lista->tamanho++;
    lista->inicio = novo;
}

void esvazia(Lista* lista) {
    Elemento* atual = lista->inicio;
    while (atual != NULL) {
        Elemento* proximo = atual->proximo;
        free(atual);
        atual = proximo;
    }
    lista->tamanho = 0;
    lista->inicio = NULL;
}