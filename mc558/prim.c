#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>

typedef struct Elemento {
    int vertice;
    struct Elemento* proximo;
} Elemento;

typedef struct Lista {
    int tamanho;
    Elemento* inicio;
} Lista;

/*
  Inicializa os campos da estrutura Lista.  Esta funcao deve ser
  chamada antes que qualquer elemento seja inserido. Complexidade:
  constante.
  Parametros:
  lista: apontador para a lista
*/
void inicializa(Lista* lista);

/*
  Insere um elemento no inicio da lista. Complexidade: constante.
  Parametros:
  lista: apontador para a lista
  vertice: inteiro representando o vertice a ser inserido
*/
void insere(Lista* lista, int vertice);

/*
  Remove todos os elementos da lista. Complexidade: linear no tamanho
  da lista.
  Parametros:
  lista: apontador para a lista
*/
void esvazia(Lista* lista);

void agm_prim(Lista * grafo, int * predecessor, int qtd_vertices){

	int * key, i;
	Elemento * atual;


	for (i = 0; i < qtd_vertices; i++){
		key = [1000]; // infinito
		predecessor = -1;
	}
	

	atual = grafo.

}


int main() {

    /* Inicializa lista com zero elementos */
    Lista lista;
    inicializa(&lista);

    /* Insere elementos de 0 a 5 */
    int i;
    for (i = 0; i < 6; i++)
        insere(&lista, i);

    printf("Tamanho da lista: %d\n", lista.tamanho);

    /* Percorre lista */
    Elemento* atual = lista.inicio;
    printf("Elementos:");
    while (atual != NULL) {
        printf(" %d", atual->vertice);
        atual = atual->proximo;
    }
    printf("\n");

    /* Remove todos os elementos da lista */
    esvazia(&lista);

    return 0;
}

void inicializa(Lista* lista) {
    lista->tamanho = 0;
    lista->inicio = NULL;
}

void insere(Lista* lista, int vertice) {
    Elemento* novo = (Elemento*)malloc(sizeof(Elemento));
    novo->vertice = vertice;
    novo->proximo = lista->inicio;
    lista->tamanho++;
    lista->inicio = novo;
}

void esvazia(Lista* lista) {
    Elemento* atual = lista->inicio;
    while (atual != NULL) {
        Elemento* proximo = atual->proximo;
        free(atual);
        atual = proximo;
    }
    lista->tamanho = 0;
    lista->inicio = NULL;
}