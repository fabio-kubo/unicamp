import os
import pywt
from Subject import Subject
from Trial import Trial


def get_training_data():
    dataPath = '/home/fabiotkkubo/unicamp/SMNI_CMI_TRAIN'
    return get_data(dataPath)

def get_test_data():
    dataPath = '/home/fabiotkkubo/unicamp/SMNI_CMI_TEST'
    return get_data(dataPath)

def get_data(dataPath):

    subjects = []
    
    for root, dirs, files in os.walk(dataPath, topdown=False):
        for name in files:
            #print(os.path.join(root, name))
            subject, trial = read_File(os.path.join(root, name))

            aux = filter(lambda x: x.id == subject.id, subjects)
            if len(aux) == 1:
                aux[0].addTrial(trial)
            else:
                subject.addTrial(trial)
                subjects.append(subject)

    return subjects

def read_File(filename):

    aux_dict = {}
    
    #wavelet transformed data
    signal_dict = {}

    eegFile = open(filename)
    fileContent = eegFile.readlines()
    
    #information separation
    subject_id = fileContent[0].split()[1]
    matching_condition = fileContent[3].split()[1] + ' ' + fileContent[3].split()[2].replace(",","")

    is_alcoholic = subject_id[3] == 'a'
    trial_id = filename.split('/')[-1]

    signals = fileContent[4:]

    for line in signals:
        if line[0] != '#':
            words = line.split()
            #if the sensor does not exist
            if words[1] not in aux_dict:
                aux_dict[words[1]] = []
            aux_dict[words[1]].append(float(words[3]))

    #for each sensor, we will have 6 lists of coefficients
    for key in aux_dict:
        signal_dict[key] = []
        cA5, cD5, cD4, cD3, cD2, cD1 = pywt.wavedec(aux_dict[key], 'db4', level=5)

        signal_dict[key].append(cA5)
        signal_dict[key].append(cD5)
        signal_dict[key].append(cD4)
        signal_dict[key].append(cD3)
        signal_dict[key].append(cD2)
        signal_dict[key].append(cD1)
                
    trial = Trial(trial_id, matching_condition, signal_dict)
    subject = Subject(subject_id, is_alcoholic)

    return subject, trial