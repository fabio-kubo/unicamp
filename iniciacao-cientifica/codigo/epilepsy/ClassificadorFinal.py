import DataEEGDataReader
import Features
from sklearn.svm import SVC
from sklearn.ensemble import RandomForestClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.cross_validation import train_test_split
from sklearn.metrics import accuracy_score, f1_score, precision_score, recall_score


#Getting the EEG data
data_wavelet_A, data_wavelet_B, data_wavelet_C, data_wavelet_D, data_wavelet_E = DataEEG.get_data_wavelet_form()

#Extracting features
features_matrix = []

for data in [data_wavelet_A, data_wavelet_B, data_wavelet_C, data_wavelet_D, data_wavelet_E]:
	for i in range(0,100):
	
		#get the ith element from each band
		coefficients = [band[i] for band in data]
		area = Features.get_area(coefficients)
		normalizedDecay = Features.get_normalizedDecay(coefficients)
		lineLength = Features.get_lineLength(coefficients)
		meanEnergy = Features.get_meanEnergy(coefficients)
		peakAmplitude = Features.get_averagePeakAmplitude(coefficients)
		valleyAmplitude = Features.get_averageValleyAmplitude(coefficients)
		normalizedPeakNumber = Features.get_normalizedPeakNumber(coefficients)
		peakVariation = Features.get_peakVariation(coefficients)
		rootMeanSquare = Features.get_rootMeanSquare(coefficients)
		waveletEnergy = Features.get_waveletEnergy(coefficients)
		standartVariation = Features.get_standartVariation(coefficients)
	
		features_matrix.append(area + normalizedDecay + lineLength + meanEnergy + peakAmplitude+valleyAmplitude + 
			peakVariation + rootMeanSquare + waveletEnergy + standartVariation);
	
#-------------------Training the svm---------------------------------------

#Creating labels: 0-healthy e 1-epileptic
labels = [0] * 200 + [1] * 300

#Splitting data in test and training dataset
data_training, data_test, labes_training, labels_test = train_test_split(features_matrix, labels, test_size=0.3, random_state=42)

#---------------------Classificador---------------------

clf_svm = SVC(kernel="poly")
clf_svm.fit(data_training, labes_training)
labels_resultado_svm = clf_svm.predict(data_test)

accuracy_obtained = accuracy_score(labels_test, labels_resultado_svm)

print '------------------SVM------------------'
print 'Accuracy: {0}'.format(accuracy_obtained)
print 'f1_score: {0}'.format(f1_score(labels_test, labels_resultado_svm))
print 'Precision score: {0}'.format(precision_score(labels_test, labels_resultado_svm))
print 'Recall score: {0}'.format(recall_score(labels_test, labels_resultado_svm))


clf_rf = RandomForestClassifier()
clf_rf.fit(data_training, labes_training)
labels_resultado_rf = clf_rf.predict(data_test)

accuracy_obtained = accuracy_score(labels_test, labels_resultado_rf)

print '------------------RF------------------'
print 'Accuracy: {0}'.format(accuracy_obtained)
print 'f1_score: {0}'.format(f1_score(labels_test, labels_resultado_rf))
print 'Precision score: {0}'.format(precision_score(labels_test, labels_resultado_rf))
print 'Recall score: {0}'.format(recall_score(labels_test, labels_resultado_rf))


clf_knn = KNeighborsClassifier()
clf_knn.fit(data_training, labes_training)
labels_resultado_knn = clf_knn.predict(data_test)
	
accuracy_obtained = accuracy_score(labels_test, labels_resultado_knn)

print '------------------KNN------------------'
print 'Accuracy: {0}'.format(accuracy_obtained)
print 'f1_score: {0}'.format(f1_score(labels_test, labels_resultado_knn))
print 'Precision score: {0}'.format(precision_score(labels_test, labels_resultado_knn))
print 'Recall score: {0}'.format(recall_score(labels_test, labels_resultado_knn))