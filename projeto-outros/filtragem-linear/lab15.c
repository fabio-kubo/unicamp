/*
	Laboratorio 15
	Filtragem Linear

	Esse programa realiza uma convolucao em imagens na escala de cinza.
	O programa percorre cada posicao da imagem e aplica a convolucao.
	Utizei dois fors para percorrer a matriz e apliquei a convolucao do filtro (apenas multiplicava se o indice fosse valido)

	Aluno: Gabriel Seixas Disselli
	Ra: 146165
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

//metodo que aloca uma matriz de inteiro na memoria
int ** alocaMatriz(int altura, int largura){
	int ** matriz, i;

	matriz = (int **) malloc(sizeof(int *) * altura);
	
	for (i = 0; i < altura; i++){
		matriz[i] = (int*) malloc(sizeof(int)*largura);
	}

	return matriz;
}

//metodo que imprime o resultado
void imprimir_resultado(int ** imagem, int largura, int altura){

	int i, j;

	printf("P2\n");
	printf("%d %d\n", largura, altura);
	printf("255\n");

	for (i = 0; i < altura; i++){
		for (j = 0; j < largura; j++){
			printf("%d ", imagem[i][j]);
		}
		printf("\n");
	}
}

//metodo que retorna o valor de 0 a 255. Caso seja negativo, retorna 0; Caso seja maior que 255, retorna 255; 
//Caso contrario, retorna o proprio numero
int retorna_valor_valido(int valor){

	if(valor < 0){
		return 0;
	}
	else if(valor > 255){
		return 255;
	}

	return valor;
}

//metodo que realiza a convolucao
int ** aplicarConvolucao(int ** imagem, int larguraImagem, int alturaImagem, int ** filtro, int linhasFiltro, int colunasFiltro){

	int ** imagemResultado, i, j, soma, n, m, p,q, tamanhoAlturaMetade, tamanhoLarguraMetade;

	tamanhoAlturaMetade = linhasFiltro/2;
	tamanhoLarguraMetade = colunasFiltro/2;

	//aloca matriz da imagem Resultante
	imagemResultado = alocaMatriz(alturaImagem, larguraImagem);

	//caso normal: filtro pode ser aplicado sem problemas
	for (i = 0; i < alturaImagem; i++){
		for (j = 0; j < larguraImagem; j++){

			soma = 0;

			for (m = 0, p =-tamanhoAlturaMetade; m < linhasFiltro; m++, p++){
				for (n = 0, q=-tamanhoLarguraMetade; n < colunasFiltro; n++, q++){

					//so faz a multiplicacao se for dentro da matriz
					if( ((i+p) >= 0 && (i+p) < alturaImagem) 
					    && ((j+q) >= 0 && (j+q) < larguraImagem) ){
						soma+= filtro[m][n]*imagem[i+p][j+q];
					}
				}
			}

			imagemResultado[i][j] = retorna_valor_valido(soma); 
		}
	}

	return imagemResultado;
}

int main(int argc, char *argv[]){

	int ** imagem, **filtro, ** resultado, largura, altura, valorMaximo, i, j, colunas, linhas;
	char aux[600];
	FILE * arquivoImagem, * arquivoFiltro;

	//verifica se esta sendo passado os dois parametros 
	if(argc <=2){
		printf("Está faltando um parâmetro\n");
		exit(EXIT_FAILURE);
	}	

	//le os arquivos de entrada
	arquivoImagem = fopen(argv[1],"r");
	arquivoFiltro = fopen(argv[2],"r");

	if(!arquivoImagem || !arquivoFiltro){
		printf("Erro ao abrir o arquivo.\n");
		exit(EXIT_FAILURE);
	}

	//descarta a primeira linha
	fscanf(arquivoImagem, "%s\n", aux);

	//le as dimensoes
	fscanf(arquivoImagem, "%d %d\n", &largura, &altura);

	//le o valor maximo (n vai precisar usar, pois o maior valor vai ser sempre 255)
	fscanf(arquivoImagem, "%d\n", &valorMaximo);

	//aloca a matriz da imagem na memoria
	imagem = alocaMatriz(altura, largura);

	//preenche a matriz
	for (i = 0; i < altura; i++){
		for (j = 0; j < largura; j++){
			fscanf(arquivoImagem, "%d ", &imagem[i][j]);
		}
	}

	//le as quantidades de colunas e linhas
	fscanf(arquivoFiltro, "%d %d\n", &colunas, &linhas);

	//aloca a matriz do filtro na memoria
	filtro = alocaMatriz(linhas, colunas);

	//realiza a leitura do filtro
	for (i = 0; i < linhas; i++){
		for (j = 0; j < colunas; j++){
			fscanf(arquivoFiltro, "%d ", &filtro[i][j]);
		}
	}

	//aplica a convolucao na imagem
	resultado = aplicarConvolucao(imagem, largura, altura, filtro, linhas, colunas);
	
	//imprime o resultado
	imprimir_resultado(resultado, largura, altura);

	//desaloca matriz da imagem
	for (i = 0; i < altura; i++){
		free(imagem[i]);
	}
	free(imagem);

	//desaloca matriz do filtro
	for (i = 0; i < linhas; i++){
		free(filtro[i]);
	}
	free(filtro);

	//desaloca matriz do resultado
	for (i = 0; i < altura; i++){
		free(resultado[i]);
	}
	free(resultado);

	return 0;
}