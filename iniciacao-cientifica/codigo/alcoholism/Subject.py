class Subject:

    def __init__(self, id, isAlcoholic):
        self.id = id
        self.isAlcoholic = isAlcoholic
        self.trials_S1Obj = []
        self.trials_S2Match = []
        self.trials_S2NoMatch = []

    def addTrial(self, trial):  
        if trial.matching_condition == "S1 obj":
            self.trials_S1Obj.append(trial)
        elif trial.matching_condition == "S2 match":
            self.trials_S2Match.append(trial)
        elif trial.matching_condition == "S2 nomatch":
            self.trials_S2NoMatch.append(trial)
        else:
            print "Erro: matching_condition invalido: " + trial.matching_condition