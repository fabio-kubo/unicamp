import java.io.*;
import java.net.*;

public class Servidor{

   private static class ProcessaMensagem implements Runnable{
      
      private DatagramPacket pacoteRecebido;
      private DatagramSocket serverSocket;
      private Correcao[] estatistica;

      public ProcessaMensagem(DatagramPacket pct, DatagramSocket socket, Correcao[] e){
         this.pacoteRecebido = pct;
         this.serverSocket = socket;
         this.estatistica = e;
      }

      public Correcao corrigir(Resposta resposta) throws Exception{

         String respostaCerta;
         switch(resposta.getIdQuestao()){
            case 1:
               respostaCerta = "VFV";
               break;
            case 2:
               respostaCerta = "FFVV";
               break;
            case 3:
               respostaCerta = "FVFVF";
               break;
            case 4:
               respostaCerta = "FFFF";
               break;
            case 5:
               respostaCerta = "VVV";
               break;
            default: 
               throw new Exception("Erro: Questão não existente");
         }

         if(respostaCerta.length() != resposta.getQtdAlternativas()){
            throw new Exception("Erro: quantidade de alternativas invalida");
         }

         if(respostaCerta.length() != resposta.getResposta().length()){
            throw new Exception("Erro: o total de alternativas escritas é diferente da quantidade de alternativas descrita");
         }

         int acertos = 0;
         int erros = 0;

         for (int i=0; i < respostaCerta.length(); i++) {
            if(respostaCerta.charAt(i) == resposta.getResposta().charAt(i)){
               acertos++;
            }
            else{
               erros++;
            }
         }

         return new Correcao(resposta.getIdQuestao(), acertos, erros);
      }
      
      public synchronized void atualizarEstatistica(Correcao correcao){
         
         estatistica[correcao.getIdQuestao() - 1].setQtdAcertos(
               estatistica[correcao.getIdQuestao() - 1].getQtdAcertos() + correcao.getQtdAcertos());
         
         estatistica[correcao.getIdQuestao() - 1].setQtdErros(
               estatistica[correcao.getIdQuestao() - 1].getQtdErros() + correcao.getQtdErros());
      }

      public void run(){

         try{
            byte[] mensagemBytes;

            ByteArrayInputStream in = new ByteArrayInputStream(pacoteRecebido.getData());
            ObjectInputStream is = new ObjectInputStream(in);

            Resposta resposta = (Resposta) is.readObject();          
            System.out.println("Recebeu resposta: " + resposta.toString());

            InetAddress IPAddress = this.pacoteRecebido.getAddress();
            int porta = this.pacoteRecebido.getPort();

            Correcao correcao = corrigir(resposta);
            atualizarEstatistica(correcao);

            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            ObjectOutputStream objOutStream = new ObjectOutputStream(outputStream);
            objOutStream.writeObject(correcao);

            //converte para bytes
            mensagemBytes = outputStream.toByteArray();

            //Envia a mensagem para o cliente
            DatagramPacket pacoteEnvio = new DatagramPacket(mensagemBytes, mensagemBytes.length, IPAddress, porta);
            serverSocket.send(pacoteEnvio);
         }
         catch(Exception e){
            System.out.println(e.getMessage());
            System.exit(0);
         }
      }
   }

   public static void imprimirQuestoes(){

      System.out.println("Questões existentes:");
      System.out.println("Q1: 3 alternativas, respostas:VFV");
      System.out.println("Q2: 4 alternativas, respostas:FFVV");
      System.out.println("Q3: 5 alternativas, respostas:FVFVF");
      System.out.println("Q4: 4 alternativas, respostas:FFFF");
      System.out.println("Q5: 3 alternativas, respostas:VVV");
   }
     
   public static void main(String[] args) throws Exception{
      DatagramSocket serverSocket = new DatagramSocket(9876);
      byte[] mensagemRecebidaBytes;
      Correcao[] estatistica = new Correcao[5];
      
      //inicializa a estatistica
      for (int i = 0; i < 5; i++) {
         estatistica[i] = new Correcao(i+1, 0, 0);
      }

      //faz com que mostre a estatistica ao encerrar o programa
      Runtime.getRuntime().addShutdownHook(new Thread() {
         @Override
         public void run() {
            System.out.println("Estatística");

            for (int i = 0; i < 5 ; i++) {
               System.out.println(estatistica[i].toStringFormatada());
            }
         }
      });

      //informa as questões que estão no banco
      imprimirQuestoes();

      System.out.println("...Server funcionando");
      //executa forever
      for(;;){
         mensagemRecebidaBytes = new byte[1024];
         
         //recebe mensagem do cliente
         DatagramPacket pacoteRecebido = new DatagramPacket(mensagemRecebidaBytes, mensagemRecebidaBytes.length);
         serverSocket.receive(pacoteRecebido);

         Thread thread = new Thread(new ProcessaMensagem(pacoteRecebido, serverSocket, estatistica));
         thread.start();
      }
  }
}