//	ALUNO: Carolina Clepf Pagotto- RA: 157770
//	Lab 15
//	
//	O programa realiza uma convolucao em uma imagem.
//	Primeiro, o programa le o arquivo da imagem e guarda as informacoes em uma matriz.
//	Realiza o mesmo com o filtro.
//	Depois aplica a convolucacao, percorrendo a matriz com dois fors, e multiplicando os valores
//	da matriz com seus respectivos do filtro.
//-----------------------------------------------------------------------------------------------------------
#include <stdlib.h>
#include <stdio.h>

//Metodo que imprime a imagem
void imprimir_imagem(int imagem[600][600], int largura, int altura);

//metodo que executa a convolucao
void executarConvolucao(int resultado[600][600], int imagem[600][600], int filtro[15][15], int larguraIm, int alturaIm, int qtdLinhasFiltro, int qtdColunasFiltro);

//metodo que verifica se o indice esta dentro do intervalo da matriz
int ehIndiceValido(int indice, int tamanho);

int main(int argc, char *argv[]){

	int imagem[600][600], filtro[15][15], resultado[600][600], n, m, q, p;
	FILE * arqImagem, * arqFiltro;
	char descarte[5];
	int i,j;


	//abre arquivo da imagem
	arqImagem = fopen(argv[1],"r");

	if(!arqImagem){
		printf("Erro ao abrir o arquivo de imagem.\n");
		exit(EXIT_FAILURE);
	}

	//le a primeira linha - descarta
	fscanf(arqImagem, "%s\n", descarte);

	//le as dimensoes
	fscanf(arqImagem, "%d %d\n", &n, &m);

	//le a terceira linha - descarta
	fscanf(arqImagem, "%s\n", descarte);

	//preenche a matriz
	for (i = 0; i < m; i++){
		for (j = 0; j < n; j++){
			fscanf(arqImagem, "%d ", &imagem[i][j]);
		}
	}

	//abre arquivo do filtro
	arqFiltro = fopen(argv[2],"r");
	if(!arqImagem){
		printf("Erro ao abrir o arquivo do filtro.\n");
		exit(EXIT_FAILURE);
	}

	//le a dimensao do filtro
	fscanf(arqFiltro, "%d %d\n", &q, &p);
	
	//carrega filtro
	for (i = 0; i < p; i++){
		for (j = 0; j < q; j++){
			fscanf(arqFiltro, "%d ", &filtro[i][j]);
		}
	}

	//executa a convolucao na imagem
	executarConvolucao(resultado, imagem, filtro, n, m, p, q);

	//imprime a imagem
	imprimir_imagem(resultado, n, m);
	
	return 0;
}

void imprimir_imagem(int imagem[600][600], int largura, int altura){

	int i, j;

	//imprime o cabecalho
	printf("P2\n%d %d\n255\n", largura, altura);

	//imprime a matriz da imagem
	for (i = 0; i < altura; i++){
		for (j = 0; j < largura; j++){
			printf("%d ", imagem[i][j]);
		}
		printf("\n");
	}
}

void executarConvolucao(int resultado[600][600], int imagem[600][600], int filtro[15][15], int larguraIm, int alturaIm, int qtdLinhasFiltro, int qtdColunasFiltro){

	int i, j, n,m, acumulado, pImagem, qImagem;

	//precorre todos os pixels
	for (i = 0; i < alturaIm; i++){
		for (j = 0; j < larguraIm; j++){
			//zera o acumulado
			acumulado = 0;

			//indice inicial da linha da imagem
			pImagem = i - (qtdLinhasFiltro/2);
			//percorre o filtro e multiplica pela posicao respectiva da imagem
			for (m = 0; m < qtdLinhasFiltro; m++){

				//indice inicial da coluna da imagem
				qImagem = j-qtdColunasFiltro/2;

				for (n = 0; n < qtdColunasFiltro; n++){

					//Caso os indices forem validos
					if(ehIndiceValido(pImagem, alturaIm) && ehIndiceValido(qImagem, larguraIm)){
						acumulado += filtro[m][n]*imagem[pImagem][qImagem];
					}

					qImagem = qImagem + 1;
				}

				pImagem = pImagem + 1;
			}

			if(acumulado < 0){
				resultado[i][j] = 0;
			}
			else if(acumulado > 255){
				resultado[i][j] = 255;
			}
			else{
				resultado[i][j] = acumulado; 	
			}
		}
	}
}

int ehIndiceValido(int indice, int tamanho){
	int valido;

	valido = (indice >= 0 && indice < tamanho);
	
	return valido;
}